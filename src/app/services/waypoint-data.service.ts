import { Injectable, OnDestroy } from '@angular/core';
import { Subscription, ReplaySubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';

interface WaypointData {
  type: string;
  features: WaypointFeature[];
}

interface WaypointFeature {
  type: string; // "Feature"
  id: string; // guid
  geometry: {
    type: string; // "Point"
    coordinates: number[]; // [-111.74133508,36.66550163,910.011]
  };
  properties: {
    cmt: string; // Dup of desc
    sym: string; // Name of indicator, e.g. "Danger Area"
    desc: string; // description of item, e.g. "Redneck Rapid (3)"
    name: string; // Name of item, e.g. "R-REDNECK"
    type: string; // type of item, e.g. "Rapid"
  };
}

@Injectable({
  providedIn: 'root'
})
export class WaypointDataService implements OnDestroy {
  public serviceData$: ReplaySubject<WaypointFeature[]> = new ReplaySubject<WaypointFeature[]>(1);
  private svcSub: Subscription;

  constructor(private http: HttpClient) {
    this.svcSub = this.http.get<WaypointData>('../../assets/data/Grand_Canyon_Features.geojson').subscribe( waypointData => {
      this.serviceData$.next(waypointData.features);
    });
  }
  ngOnDestroy() {
    this.unsub();
  }
  private unsub() {
    if (this.svcSub && !this.svcSub.closed) {
      this.svcSub.unsubscribe();
    }
  }
}
