import { Injectable, OnDestroy } from '@angular/core';
import { Subscription, ReplaySubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { WaypointDataService } from './waypoint-data.service';
import { map } from 'rxjs/operators';
import { MapItem } from './interfaces';

@Injectable({
  providedIn: 'root'
})
export class MapDataService implements OnDestroy {
  static SpecialPoints = {
    LeesFerryCamp: 'I10',
    DiamondCreekTakeout: 'I6250'
  };
  static HUALAPAI_LAND_START = 165.2;
  static HUALAPAI_LAND_END = 273.9;
  static DIAMOND_MILE_MARKER = 225.9;
  static PEARCE_MILE_MARKER = 281;

  public static STORAGE_KEY = 'mapData';
  public serviceData: MapItem[];
  public serviceData$: ReplaySubject<MapItem[]>;
  private svcSub: Subscription;
  getItem(id: string): MapItem {
    return this.serviceData.find(md => md.id === id);
  }

  constructor(private http: HttpClient,
              private waypointDataService: WaypointDataService) {
    this.serviceData$ = new ReplaySubject<MapItem[]>(1);
    waypointDataService.serviceData$.subscribe(wd => {
      this.svcSub = this.http.get<MapItem[]>('../../assets/data/mapData.json')
      .pipe(
        map(md => md as MapItem[]),
        map(md => {
          md.forEach(mi => {
            let wptSearchKey = '';
            let wptTypeFilter = '';
            let wi;
            switch (mi.type) {
              case 'C': {
                wptTypeFilter = 'Campground';
                wptSearchKey = mi.name.replace(' Camp', '');
                if (mi.subTypes[0] === 'H') {
                  wptSearchKey += ' (H)';
                }
                wi = wd.find(item => item.properties.type === wptTypeFilter && item.properties.desc === wptSearchKey);
                break;
              }
              case 'R': {
                wptTypeFilter = 'Rapid';
                wi = wd.find(item => item.properties.type === wptTypeFilter && item.properties.desc?.startsWith(mi.name));
                break;
              }
              default: {
                break;
              }
            }
            mi.wpt = wi;
          });
          return md;
        })
      )
      .subscribe(mapData => {
        this.serviceData = mapData;
        this.serviceData$.next(mapData);
      });
    });
  }
  ngOnDestroy() {
    this.unsub();
  }
  private unsub() {
    if (!this.serviceData$.closed) {
      this.serviceData$.unsubscribe();
    }
    if (this.svcSub && !this.svcSub.closed) {
      this.svcSub.unsubscribe();
    }
  }
}
