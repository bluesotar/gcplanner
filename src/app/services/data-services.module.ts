import { NgModule } from '@angular/core';
import { TideTablesService } from './tide-tables.service';
import { MapDataService } from './map-data.service';
import { MoonService } from './moon.service';
import { SunDataService } from './sun-data.service';
import { DutyRosterService } from './duty-roster.service';
import { TripConfigService } from './trip-config.service';
import { AppDataService } from './app-data.service';
import { NoteService } from './note.service';
import { SelectionsService } from './selections.service';
import { WaypointDataService } from './waypoint-data.service';
import { ItemFiltersService } from './item-filters.service';
import { AppConfiguredGuard } from './app-configured.guard';

@NgModule({
  providers: [
    MapDataService,
    MoonService,
    SunDataService,
    TideTablesService,
    WaypointDataService
  ]
})
export class DataServicesStaticModule { }

@NgModule({
  providers: [
    DutyRosterService,
    TripConfigService,
    AppDataService,
    NoteService,
    SelectionsService,
    ItemFiltersService,
    AppConfiguredGuard
  ]
})
export class DataServicesStorageModule { }
