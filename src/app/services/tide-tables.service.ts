import { Injectable, OnDestroy } from '@angular/core';
import { Subscription, ReplaySubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { TidePlace } from './interfaces';

@Injectable({
  providedIn: 'root'
})
export class TideTablesService implements OnDestroy {

  public serviceData$: ReplaySubject<TidePlace[]> = new ReplaySubject<TidePlace[]>(1);
  private svcSub: Subscription;

  constructor(private http: HttpClient) {
    this.svcSub = this.http.get<TidePlace[]>('../../assets/data/tideTables.json').subscribe(tideData => {
      this.serviceData$.next(tideData);
    });
  }

  ngOnDestroy() {
    this.unsub();
  }
  private unsub() {
    if (this.svcSub && !this.svcSub.closed) {
      this.svcSub.unsubscribe();
    }
  }

}
