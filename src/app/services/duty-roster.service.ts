import { Injectable, OnDestroy } from '@angular/core';
import { Subscription, ReplaySubject, Observable, of } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { StorageKeys, AppDataService } from './app-data.service';
import { DutyData, DutySet, DutyRosterItem, BoaterTeam, TripConfig } from './interfaces';
import { TripConfigService } from './trip-config.service';

@Injectable({
  providedIn: 'root'
})
export class DutyRosterService implements OnDestroy {
  public serviceData$: ReplaySubject<DutyData> = new ReplaySubject<DutyData>(1);
  private svcSub: Subscription;
  public dutyData: DutyData = {} as DutyData;
  daysOnTheWater: number;

  constructor(private appDataService: AppDataService,
              private configService: TripConfigService) {

    this.configService.serviceData$.subscribe(config => {
      this.daysOnTheWater = config.days;
    });
    this.svcSub = this.loadData().subscribe(roster => {
      // console.log(`roster data is ${JSON.stringify(roster)}`);
      this.dutyData = roster;
      this.updateFeeds(roster);
    });
  }
  ngOnDestroy() {
    this.unsub();
  }
  private unsub() {
    if (this.svcSub && !this.svcSub.closed) {
      this.svcSub.unsubscribe();
    }
  }

  public loadData(): Observable<DutyData> {
    return this.appDataService.getTripItem(StorageKeys.Duties)
      .pipe(
//        tap(data => { console.log(`duty data is ${JSON.stringify(data)}`); return data; }),
        map(data => {
          // console.log(`duty data is ${JSON.stringify(data)} and the condition is ${!('configured' in data)}`);
          if (!('configured' in data)) {
            data = this.initializeRosterData(this.daysOnTheWater);
          }
          return data;
        })
      );
  }

  public initializeRosterData(days): DutyData {
    // console.log(`Initializing the duty data`);
    // const roster: DutyRosterItem[] = [
    //     {
    //       day: 1,
    //       set: 'Food',
    //       team: 'A'
    //     },
    //     {
    //       day: 1,
    //       set: 'NotFood',
    //       team: 'B'
    //     },
    //     {
    //       day: 2,
    //       set: 'Food',
    //       team: 'B'
    //     },
    //     {
    //       day: 2,
    //       set: 'NotFood',
    //       team: 'A'
    //     },
    //     {
    //       day: 3,
    //       set: 'Food',
    //       team: 'A'
    //     },
    //     {
    //       day: 3,
    //       set: 'NotFood',
    //       team: 'B'
    //     },
    //     {
    //       day: 4,
    //       set: 'Food',
    //       team: 'B'
    //     },
    //     {
    //       day: 4,
    //       set: 'NotFood',
    //       team: 'A'
    //     },
    //     {
    //       day: 5,
    //       set: 'Food',
    //       team: 'A'
    //     },
    //     {
    //       day: 5,
    //       set: 'NotFood',
    //       team: 'B'
    //     },
    //     {
    //       day: 6,
    //       set: 'Food',
    //       team: 'B'
    //     },
    //     {
    //       day: 6,
    //       set: 'NotFood',
    //       team: 'A'
    //     },
    //     {
    //       day: 7,
    //       set: 'Food',
    //       team: 'A'
    //     },
    //     {
    //       day: 7,
    //       set: 'NotFood',
    //       team: 'B'
    //     },
    //     {
    //       day: 8,
    //       set: 'Food',
    //       team: 'B'
    //     },
    //     {
    //       day: 8,
    //       set: 'NotFood',
    //       team: 'A'
    //     },
    //     {
    //       day: 9,
    //       set: 'Food',
    //       team: 'A'
    //     },
    //     {
    //       day: 9,
    //       set: 'NotFood',
    //       team: 'B'
    //     },
    //     {
    //       day: 10,
    //       set: 'Food',
    //       team: 'B'
    //     },
    //     {
    //       day: 10,
    //       set: 'NotFood',
    //       team: 'A'
    //     },
    //     {
    //       day: 11,
    //       set: 'Food',
    //       team: 'A'
    //     },
    //     {
    //       day: 11,
    //       set: 'NotFood',
    //       team: 'B'
    //     },
    //     {
    //       day: 12,
    //       set: 'Food',
    //       team: 'B'
    //     },
    //     {
    //       day: 12,
    //       set: 'NotFood',
    //       team: 'A'
    //     },
    //     {
    //       day: 13,
    //       set: 'Food',
    //       team: 'A'
    //     },
    //     {
    //       day: 13,
    //       set: 'NotFood',
    //       team: 'B'
    //     },
    //     {
    //       day: 14,
    //       set: 'Food',
    //       team: 'B'
    //     },
    //     {
    //       day: 14,
    //       set: 'NotFood',
    //       team: 'A'
    //     },
    //     {
    //       day: 15,
    //       set: 'Food',
    //       team: 'A'
    //     },
    //     {
    //       day: 15,
    //       set: 'NotFood',
    //       team: 'B'
    //     },
    //     {
    //       day: 16,
    //       set: 'Food',
    //       team: 'B'
    //     },
    //     {
    //       day: 16,
    //       set: 'NotFood',
    //       team: 'A'
    //     }
    // ];
    const roster: DutyRosterItem[] = [];
    // for (let x = 1; x <= this.daysOnTheWater; x++) {
    //   roster.push({
    //     day: x
    //   });
    // }
    const duties: DutySet[] = [
      {
        duty: 'Dinner',
        active: true,
        set: 'Food',
        setDescription: 'Food crew gets first pick of campsites. Setup kitchen. Kitchen teardown is group activity.',
        description: 'Prepare dinner. Set up the trash and recycle bags. Smash cans as needed. Close up and secure it overnight. This should be done after the dinner dish water is strained. Set up dish line.'
      },
      {
        duty: 'Breakfast',
        active: true,
        set: 'Food',
        description: 'Up early to make coffee, then breakfast. Prep dish line.'
      },
      {
        duty: 'Lunch',
        active: true,
        set: 'Food',
        description: 'Prep lunch during breakfast. Serve. Pull out dirty dishes upon arrival at camp. Ensure there is no need to access a cooler at lunch time.'
      },
      {
        duty: 'Groover',
        active: true,
        set: 'Groover',
        description: 'Groover setup, takedown and upkeep'
      },
      {
        duty: 'Anchor',
        active: true,
        set: 'Not Food',
        description: 'Secure the boats for the night (tie to the canyon)'
      },
      {
        duty: 'Ashes',
        active: true,
        set: 'Not Food',
        description: 'If we are permitted a fire, cleanup of the fire pan'
      },
      {
        duty: 'Water',
        active: true,
        set: 'Not Food',
        description: 'Ensure we have enough water. Filter more water, if needed'
      }
    ];
    const teams: BoaterTeam[] = [
      // {
      //   boater: 'Nick',
      //   team: 'A',
      //   color: 'Orange'
      // },
      // {
      //   boater: 'Lisa',
      //   team: 'A',
      //   color: 'Orange'
      // },
      // {
      //   boater: 'Alex',
      //   team: 'A',
      //   color: 'Orange'
      // },
      // {
      //   boater: 'Carl',
      //   team: 'A',
      //   color: 'Orange'
      // },
      // {
      //   boater: 'Dan',
      //   team: 'A',
      //   color: 'Orange'
      // },
      // {
      //   boater: 'Laurie',
      //   team: 'B',
      //   color: 'Green'
      // },
      // {
      //   boater: 'Solly',
      //   team: 'B',
      //   color: 'Green'
      // },
      // {
      //   boater: 'Karin',
      //   team: 'B',
      //   color: 'Green'
      // },
      // {
      //   boater: 'Craig',
      //   team: 'B',
      //   color: 'Green'
      // },
      // {
      //   boater: 'Serena',
      //   team: 'B',
      //   color: 'Green'
      // },
    ];
    const data: DutyData = {
      duties,
      teams,
      roster,
      configured: true
    };

    return data;
  }

  private updateFeeds(newServiceData: DutyData) {
    this.serviceData$.next(newServiceData);
  }

  public saveData(newServiceData: DutyData) {
    this.appDataService.saveTripItem(newServiceData, StorageKeys.Duties);
    this.updateFeeds(newServiceData);
  }
}
