import { SafeHtml } from '@angular/platform-browser';

export const SUBJECT_BUFFER_SIZE = 1;
export const DEFAULT_TRIP_NAME = '__gcpDefaults';

export interface MapItemType {
  name: string;
  code: string;
  class: string;
  desc: string;
}

export interface Waypoint {
  lat: string;
  lon: string;
  ele: string;
  name: string;
  cmt: string;
  desc: string;
  sym: string;
  type: string;
  extensions: {
    label: {
      label_text: string;
    }
  };
}

export interface MapItem {
  id: string;
  type: string; // C-amp, R-apid, H-ike, P-oi, W-ater, S-idecanyon, M-anmade, G-eology, F-watersource
  subTypes: string[]; // P=Favorite, E=Exch, H=Hualapai?, B=Below res
  mile: number;
  name: string;
  side?: string; // R-ight, L-eft
  size?: string; // S, M, L, 1-10
  hike?: {
      number: number; // in DHftR
      duration?: string; // short, med, long
      difficulty?: string; // easy, med, hard, insane
      description?: string;
  };
  waypointName?: string;
  notes?: string;
  constraints?: string; // Exchange, Hualapai...
  altName?: string;
  page?: number;
  wpt?: {
    type: string; // "Feature"
    id: string; // guid
    geometry: {
      type: string; // "Point"
      coordinates: number[]; // [-111.74133508,36.66550163,910.011]
    },
    properties: {
      cmt: string; // Dup of desc
      sym: string; // Name of indicator, e.g. "Danger Area"
      desc: string; // description of item, e.g. "Redneck Rapid (3)"
      name: string; // Name of item, e.g. "R-REDNECK"
      type: string; // type of item, e.g. "Rapid"
    }
  };
}
export interface PhaseData {
  date: number; // I don't think this is actually in the data
  phaseName: string; // "Full Moon", "Waning", "Last Quarter", "New Moon", "Waxing", "First Quarter"
  isPhaseLimit: boolean;
  lighting: number;
  svg: SafeHtml; // svg element node
  svgMini: boolean;
  timeEvent: boolean;
  dis: number;
  dayWeek: number; // 0-6 Day of the week
  npWidget: string; // Text of image; e.g. "Waxing (82%)"
}

export interface MoonData {
  monthName: string; // Month name of the data requested; e.g. "September"
  firstDayMonth: string; // 0-6 day of the week that the 1st falls on
  daysMonth: string; // 28-31 number of the days in the month
  nameDay: string[]; // "Monday" to "Sunday"
  nameMonth: string[]; // "January" to "December"
  phase: PhaseData; // This object has keys from 1-days in month
  month: number; // Month of the data requested
  year: number; // Year of the data requested
  receivedVariables: {}; // request parameters
  lang: string; // short language; e.g. en
  language: string; // long language; e.g. English
  title: string[]; // ["Moon's calendar", "Moon phases"]
  nextFullMoon: string; // html for text indicating the next full moon
  autor: string; // "wdisseny.com"
  version: string; // 2
}

export interface NoteData {
  id: string;
  created: Date;
  itemName: string;
  itemType: string;
  text: string;
  lastUpdate: Date;
}

export interface TidePlace {
  page: string;
  mile: number;
  place: string;
  tides: {
    at4mph: {
      delay: string;
      rise: string;
    },
    at43mph: {
      delay: string;
      rise: string;
    },
    at45mph: {
      delay: string;
      rise: string;
    },
    at5mph: {
      delay: string;
      rise: string;
    }
  };
}

export interface Trip {
  dataVersion: number;
  type: 'tripData';
  tripName: string;
  tripStatus: TripStatus;
  created?: Date;
  deleted?: Date;
  tripData: {
    tripConfig: TripConfig;
    selections: {};
    notes: NoteData[];
    duties: DutyData;
    moon: {}; // Two keys, both date related
  };
}
export interface FavoritesSet {
  name: string;               // name of favorite list
  items: string[];            // list of mapitem ids
}
export interface GCPlannerData {
  activeTripName: string;     // current trip configuration
  trips: Trip[];          // list of trip configurations
  itemFilters: MapItemType[]; // last used set of filters
  activeFavoritesSet: string; // name of a list of favorites
  favoritesSets: FavoritesSet[];
}
export enum TripStatus {
  new,
  active,
  complete,
  deleted
}
export interface TripConfig {
  cfs?: number;
  takeoutMileMarker: number;
  exchanges: boolean;
  hualapaiPermit: boolean;
  launchDate?: Date;
  takeoutDate?: Date;
  days?: number;
  maxDays?: number;
  darkMode: boolean;
  configured: boolean;
}
export interface MapItemType {
  name: string;
  code: string;
  class: string;
  desc: string;
}

export enum TakeoutMileMarkers {
  diamond = 225.9,
  pearce = 281
}

export interface ChooserDetailsData {
  id: string;
  name: string;
}
export interface ItineraryDetailsData {
  id: string;
  name: string;
}

export interface DutySet {
  duty: string;
  set: string;
  setDescription?: string;
  active: boolean;
  description?: string;
}
export interface BoaterTeam {
  boater: string;
  team: string;
  color?: string;
}
export interface DutyPatternItem {
  day: number;
  set?: string;
  team?: string;
}
export interface DutyRosterItem {
  set?: string;
  teams?: {};
}
export interface DutyData {
  duties: DutySet[];
  teams: BoaterTeam[];
  roster: DutyRosterItem[];
  configured: boolean;
}

export class Interfaces {
  constructor() { }
}
