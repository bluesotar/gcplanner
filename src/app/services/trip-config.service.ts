import { Injectable, OnDestroy } from '@angular/core';
import { concat, Observable, ReplaySubject, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { MapDataService } from './map-data.service';
import { StorageKeys, AppDataService } from './app-data.service';
import { TakeoutMileMarkers, TripConfig } from './interfaces';

interface MaxDayItem {
  maxDOM: number;
  maxDays: number;
  toPearce: number;
}
@Injectable({
  providedIn: 'root'
})
export class TripConfigService implements OnDestroy {
  public static STORAGE_KEY = StorageKeys.TripConfig;
  public serviceData$: ReplaySubject<TripConfig>;
  private svcSub: Subscription;
  private maxDaysList: MaxDayItem[][];

  public configurationDefaults: TripConfig = {
    configured: false,
    takeoutMileMarker: MapDataService.DIAMOND_MILE_MARKER,
    exchanges: false,
    hualapaiPermit: true,
    darkMode: true,
  };

  constructor(private appDataService: AppDataService,
              private http: HttpClient) {
    this.serviceData$ = new ReplaySubject<TripConfig>(1);
    this.svcSub = concat(
      this.http.get<MaxDayItem[][]>('../../assets/data/maxDays.json').pipe(
        tap(data => this.maxDaysList = data as MaxDayItem[][])
      ),
      this.loadData().pipe(tap(data => this.updateFeeds(data)))
    ).subscribe();
  }

  ngOnDestroy() {
    this.unsub();
  }
  private unsub() {
    if (this.svcSub && !this.svcSub.closed) {
      this.svcSub.unsubscribe();
    }
  }

  private loadData(): Observable<TripConfig> {
    return this.appDataService.getTripItem(StorageKeys.TripConfig)
      .pipe(
        map(data => data === undefined ? this.configurationDefaults : data as TripConfig),
        map(config => {
          if (!config.configured) {
            config.launchDate = new Date(Date.now());
            config.maxDays = this.getMaxDays(config.launchDate, config.takeoutMileMarker);
            config.days = config.maxDays;
          } else {
            config.launchDate = new Date(config.launchDate);
          }
          config.takeoutDate = new Date();
          config.takeoutDate.setDate(config.launchDate.getDate() + config.days - 1);
          return config;
        }),
      );
  }

  private updateFeeds(newServiceData: TripConfig) {
    this.serviceData$.next(newServiceData);
  }

  public saveData(newServiceData: TripConfig) {
    newServiceData.maxDays = this.getMaxDays(newServiceData.launchDate, newServiceData.takeoutMileMarker);
    newServiceData.configured = true;
    this.appDataService.saveTripItem(newServiceData, StorageKeys.TripConfig);
    this.updateFeeds(newServiceData);
  }

  /*
    16 days (15 nights) May through August
    18 days (17 nights) Sept 1-15
    21 days (20 nights) Sept 16 - Oct 31
    25 days (24 nights) Nov 1 - Feb 29
    21 days (20 nights) Mar 1 - April 30
    getDay 0-6 su-Sa
    getDate 1-31
    getMonth 0-11
    Add 3-5 days to travel to Pierce
  */
  public getMaxDays(launchOn: Date | string, takeout: TakeoutMileMarkers): number {
    const launchDate = (typeof launchOn === 'string') ? new Date(launchOn) : launchOn as Date;

    const date = launchDate.getDate();
    const month = launchDate.getMonth();
    const maxDayItem = this.maxDaysList[month].filter((d: MaxDayItem) => d.maxDOM >= date );
    let maxDays = maxDayItem[0].maxDays;
    if (takeout === TakeoutMileMarkers.pearce) { // add more days if taking out at Pearce Ferry
      maxDays += maxDayItem[0].toPearce;
    }
    return maxDays;
  }
}
