import { Injectable, OnDestroy } from '@angular/core';
import { ReplaySubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AppDataService } from './app-data.service';
import { MapItemType } from './interfaces';

@Injectable({
  providedIn: 'root'
})
export class ItemFiltersService implements OnDestroy {

  public static MapItemTypes: MapItemType[] = [
    { name: 'All', code: '*', class: 'allItems', desc: 'All item types'},
    { name: 'Favorites', code: '!', class: 'allItems', desc: 'Favorites'},
    { name: 'Camp', code: 'C', class: 'camp-type', desc: 'Camps' },
    { name: 'Rapid', code: 'R', class: 'rapid-type', desc: 'Rapids' },
    { name: 'Hike', code: 'H', class: 'hike-type', desc: 'Hikes' },
    { name: 'Water', code: 'W', class: 'water-type', desc: 'Rivers and creeks' },
    { name: 'WaterSource', code: 'F', class: 'source-type', desc: 'Water sources' },
    { name: 'POI', code: 'P', class: 'poi-type', desc: 'Points of interest' },
    { name: 'Geology', code: 'G', class: 'geology-type', desc: 'Geologically interesting sights' },
    { name: 'Canyon', code: 'S', class: 'canyon-type', desc: 'Side canyons' },
    { name: 'Manmade', code: 'M', class: 'manmade-type', desc: 'Manmade items' },
    { name: 'Log', code: 'L', class: 'manmade-type', desc: 'Trip log'}
  ];

  private FAVORITES = 1;
  public serviceData$: ReplaySubject<MapItemType[]>;

  constructor(private appDataService: AppDataService) {
    this.serviceData$ = new ReplaySubject<MapItemType[]>(1);
    this.loadData().subscribe(filters => {
      this.serviceData$.next(filters);
    });
  }

  ngOnDestroy() {
    if (!this.serviceData$.closed) {
      this.serviceData$.unsubscribe();
    }
  }

  public loadData(): Observable<MapItemType[]> {
    return this.appDataService.getFilters()
      .pipe(
        map(data => data === undefined ? [ItemFiltersService.MapItemTypes[this.FAVORITES]] : data as MapItemType[]),
      );
  }

  public saveData(newServiceData: MapItemType[]) {
    this.appDataService.setFilters(newServiceData);
  }
}
