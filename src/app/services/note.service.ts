import { Injectable, OnDestroy } from '@angular/core';
import { Subscription, ReplaySubject, Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { MapDataService } from './map-data.service';
import { StorageKeys, AppDataService } from './app-data.service';
import { NoteData, SUBJECT_BUFFER_SIZE } from './interfaces';

@Injectable({
  providedIn: 'root'
})
export class NoteService implements OnDestroy {
  public static STORAGE_KEY = StorageKeys.Notes;
  public serviceData$: ReplaySubject<NoteData[]>;
  private notesData: NoteData[];
  private svcSub: Subscription;

  constructor(private appDataService: AppDataService,
              private mapDataService: MapDataService) {
    this.serviceData$ = new ReplaySubject<NoteData[]>(SUBJECT_BUFFER_SIZE);
    this.loadData().subscribe(notes => {
      this.updateFeeds(notes);
    });
  }

  ngOnDestroy() {
    this.unsub();
  }
  private unsub() {
    if (this.svcSub && !this.svcSub.closed) {
      this.svcSub.unsubscribe();
    }
  }
  public loadData(): Observable<NoteData[]> {
    return this.appDataService.getTripItem(StorageKeys.Notes)
      .pipe(
        map(data => this.upgrade(data)),
        tap(data => {
          this.notesData = data;
          return data;
        })
      );
  }
  public upgrade(data: any): NoteData[] {
    // if data is not found, return empty array.
    if (!data) {
      return [];
    }
    // if data is already an array, it's been
    // upgraded, so just return it.
    if (Array.isArray(data)) {
      return data;
    }

    // Here we have the old format which is just an object
    // representing a key/value array. We want additional
    // data, so we're going to convert it to an array.
    const noteData: NoteData[] = [];
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        const noteItem = {
          id: key,
          text: data[key],
          itemName: this.mapDataService.getItem(key).name,
          itemType: this.mapDataService.getItem(key).type,
          created: new Date(Date.now()),
          lastUpdate: new Date(Date.now())
        };
        noteData.push(noteItem);
      }
    }
    return noteData;
  }

  public getItem(id: string): NoteData {
    if (this.notesData === undefined) {
      this.notesData = [];
    }
    let item = this.notesData.find(nd => nd.id === id);
    if (item === undefined) {
      item = {
        id,
        text: '',
        itemName: this.mapDataService.getItem(id).name,
        itemType: this.mapDataService.getItem(id).type,
        created: new Date(Date.now()),
        lastUpdate: new Date(Date.now())
      };
    }
    return item;
  }
  public hasNote(id: string): boolean {
    if (!this.notesData) { return false; }
    return this.notesData.findIndex(nd => nd.id === id) >= 0;
  }


  public set(id: string, noteText: string) {
    let newServiceData = this.notesData;
    if (noteText === '') {
      newServiceData = newServiceData.filter(nd => nd.id !== id);
    } else {
      const index = newServiceData.findIndex(nd => nd.id === id);
      // update if item was found
      if (index >= 0) {
        const noteData = newServiceData[index];
        noteData.text = noteText;
        noteData.lastUpdate = new Date(Date.now());
        newServiceData[index] = noteData;
      } else { // otherwise add a new item.
        const noteData = {
          id,
          text: noteText,
          itemName: this.mapDataService.getItem(id).name,
          itemType: this.mapDataService.getItem(id).type,
          created: new Date(Date.now()),
          lastUpdate: new Date(Date.now())
        };
        newServiceData.push(noteData);
      }
    }
    this.saveData(newServiceData);
  }

  private updateFeeds(newServiceData: NoteData[]) {
    this.serviceData$.next(newServiceData);
  }
  public saveData(newServiceData: NoteData[]) {
    this.appDataService.saveTripItem(newServiceData, StorageKeys.Notes);
    this.notesData = newServiceData;
    this.updateFeeds(newServiceData);
  }

}
