import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription, Observable, of, ReplaySubject, forkJoin } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Network } from '@ngx-pwa/offline';
import { MoonData } from './interfaces';
import { AppDataService, StorageKeys } from './app-data.service';

@Injectable({
  providedIn: 'root'
})
export class MoonService implements OnDestroy {
  serviceData$: ReplaySubject<{}> = new ReplaySubject<{}>(1);
  private moonData: {}; // MoonData[] = [];
  private svcSub: Subscription;
  protected online = this.network.online;

  // This will generate a key to be used for accessing the local storage.
  // The format of the key is 'MoonYYYYMM' based on the date parameter.
  public static getStorageKey(date: Date): string {
    return StorageKeys.Moon + (date.getFullYear()).toString() + (date.getMonth() + 1).toString().padStart(2, '0');
  }
  // This will generate the url to get the moon phase data based on the input date's month and year.
  static getUrl(date: Date): string {
    let url = 'https://www.icalendar37.net/lunar/api/?lang=en&size=25&sizeQuarter=20&texturize=false&month=';
    url += (date.getMonth() + 1).toString().padStart(2, '0') + '&year=' + (date.getFullYear()).toString();
    return url;
  }

  constructor(private http: HttpClient,
              private appDataService: AppDataService,
              protected network: Network) {
      this.loadData().subscribe();
  }

  ngOnDestroy() {
    if (this.svcSub && !this.svcSub.closed) {
      this.svcSub.unsubscribe();
    }
  }
  public changeLaunchDate(newLaunchDate: Date) {
    const thisMonth = newLaunchDate;
    const nextMonth = new Date(newLaunchDate.getFullYear(), newLaunchDate.getMonth() + 1);
    this.moonData = {};
    this.svcSub = forkJoin([
      this.getMoonData(thisMonth),
      this.getMoonData(nextMonth)
    ]).subscribe((moonData: MoonData[]) => {
      this.moonData[MoonService.getStorageKey(thisMonth)] = moonData[0];
      this.moonData[MoonService.getStorageKey(nextMonth)] = moonData[1];
      this.saveData(this.moonData);
    });
  }

  private getCachedMoonData(date: Date) {
    return this.moonData[MoonService.getStorageKey(date)] as MoonData;
  }
  private getMoonData(date: Date) {
    // Check to see if the data has already been pulled
    const moonData = this.getCachedMoonData(date);
    let retObs: Observable<MoonData>;

    if (moonData !== undefined) { // if found, use it.
      retObs = of(moonData);
    } else { // otherwise, pull from internet
      if (this.online) {
        retObs = this.http.get(MoonService.getUrl(date)).pipe(
          map(netData => {
            return this.removeLink(netData as MoonData);
          })
        );
      } else {
        retObs = of();
      } // offline
    } // else !moonData
    return retObs;
  }

  // This will remove the link from the generated SVG image
  private removeLink(data: MoonData): MoonData {
    for (const day in data.phase) {
      if (data.phase.hasOwnProperty(day)) {
        const svg = data.phase[day].svg;
        data.phase[day].svg = svg
          .replace('<a xlink:href="https://www.icalendar37.net/lunar/app/" target="_blank">', '')
          .replace('<circle cx="50" cy="50" r="49" style="pointer-events:all;cursor:pointer" stroke-width="0"   fill="transparent" />', '')
          .replace('</a>', '');
      }
    }
    return data;
  }

  private loadData(): Observable<{}> {
    return this.appDataService.getTripItem(StorageKeys.Moon).pipe(
      tap((moondata) => {
        this.updateFeeds(moondata);
        return moondata as MoonData;
      })
    );
  }
  private saveData(newServiceData: {}) {
    this.appDataService.saveTripItem(newServiceData, StorageKeys.Moon);
    this.updateFeeds(newServiceData);
  }
  private updateFeeds(newServiceData: {}) {
    this.serviceData$.next(newServiceData);
  }
}
