import { Injectable, OnDestroy } from '@angular/core';
import { ReplaySubject, Subscription } from 'rxjs';
import { HttpClient } from '@angular/common/http';

interface SunPlace {
  mile: number;
  location: string;
  sunrise: string;
  sunset: string;
  notes: string;
}

@Injectable({
  providedIn: 'root'
})
export class SunDataService implements OnDestroy {

  public serviceData$: ReplaySubject<SunPlace[][]> = new ReplaySubject<SunPlace[][]>(1);
  private svcSub: Subscription;
  private sunArray: SunPlace[][];

  constructor(private http: HttpClient) {
    this.svcSub = this.http.get<SunPlace[][]>('../../assets/data/sunData.json').subscribe( sunData => {
      this.sunArray = sunData;
      this.serviceData$.next(sunData);
    });
  }
  ngOnDestroy() {
    this.unsub();
  }
  private unsub() {
    if (this.svcSub && !this.svcSub.closed) {
      this.svcSub.unsubscribe();
    }
  }

  public getSunny(mon: number, mile: number): SunPlace {
    if (this.sunArray === undefined) {
      return undefined;
    }
    return this.sunArray[mon].find(l => l.mile === mile);
  }
}

