import { Injectable, OnDestroy } from '@angular/core';
import { ReplaySubject, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { StorageKeys, AppDataService } from './app-data.service';

@Injectable({
  providedIn: 'root'
})
export class SelectionsService implements OnDestroy {
  public static ActionName = {
    C: 'Camp',
    L: 'Layover',
    S: 'Stop',
    V: 'Stop',
    N: 'Note'
  };

//  public static STORAGE_KEY = StorageKeys.Selections;
  public serviceData$: ReplaySubject<{}>;
  private svcSub: Subscription;
  private actions: {} = {};
  public campCount$: ReplaySubject<number>;

  constructor(private appDataService: AppDataService) {
    this.serviceData$ = new ReplaySubject<{}>(1);
    this.campCount$ = new ReplaySubject<number>(1);
    this.loadData().subscribe(config => {
      this.updateFeeds(config);
    });
  }
  ngOnDestroy() {
    this.unsub();
  }
  private unsub() {
    if (this.svcSub && !this.svcSub.closed) {
      this.svcSub.unsubscribe();
    }
  }

  public loadData() {
    return this.appDataService.getTripItem(StorageKeys.Selections)
      .pipe(
        map(data => data === undefined ? {} : data),
        tap(data => {
          this.actions = data;
          this.updateFeeds(data);
          return data;
        })
      );
  }
  private updateFeeds(newServiceData: {}) {
    this.serviceData$.next(newServiceData);
    this.campCount$.next(this.getDaysUsed());
  }

  // public import(exportedData: string) {
  //   const newServiceData =  JSON.parse(exportedData)[this.storageKey];
  //   this.write(newServiceData);
  // }
  // public export(): Promise<string> {
  //   return new Promise<string>( resolve => {
  //     let exportData = '';
  //     this.serviceData$.subscribe(selectionsObject => {
  //       if (selectionsObject) {
  //         exportData += `"${this.storageKey}": ${JSON.stringify(selectionsObject, null, '')}`;
  //         resolve(exportData);
  //       }
  //     }).unsubscribe();
  //   });
  // }

  public getItem(id: string) {
    let action = '';
    if (id in this.actions) {
      action = this.actions[id];
    }
    return action;
  }

  public getDaysUsed() {
    // The +1 is because on a 21 day trip, you actually only get 20 nights, so
    // I'm counting Lee's Ferry as a night :-)
    let nights = 0;
    for (const s in this.actions) {
      if (this.actions.hasOwnProperty(s)) {
        if (this.actions[s] === 'C') {
          nights += 1;
        }
        if (this.actions[s] === 'L') {
          nights += 2;
        }
      }
    }
    return nights;
  }

  public set(id: string, action: string) {
    if (action === '') {
      delete this.actions[id];
    } else {
      this.actions[id] = action;
    }
    this.saveData(this.actions);
  }
  public saveData(newServiceData: {}) {
    const newSelectionObject = {};
    Object.keys(newServiceData)
      .map(s => parseInt(s.substr(1), 10) ) // convert to numeric
      .sort((a, b) => a - b)                // sort the numbers
      .map(n => `I${n}`)                   // put the "I" back on
      .forEach(k => newSelectionObject[k] = newServiceData[k]); // build the new selection object
    this.appDataService.saveTripItem(newServiceData, StorageKeys.Selections);
    this.actions = newServiceData;
    this.updateFeeds(newServiceData);
  }
}
