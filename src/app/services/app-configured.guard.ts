import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AppDataService } from './app-data.service';

@Injectable()
export class AppConfiguredGuard implements CanActivate {
    constructor(private appDataService: AppDataService, private router: Router) { }

    canActivate(routeSnapshot: ActivatedRouteSnapshot): Observable<boolean> {
        const customRedirect = routeSnapshot.data.appConfiguredGuardRedirect;
        return this.appDataService.getTrip()
        .pipe(
            map( trip => trip?.tripData?.tripConfig?.configured ),
            tap( configured => {
                if (!configured){
                    this.router.navigate([customRedirect]);
                }
            })
        );
    }
}
