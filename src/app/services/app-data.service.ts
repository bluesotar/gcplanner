import { Injectable, OnDestroy } from '@angular/core';
import { StorageMap } from '@ngx-pwa/local-storage';
import { forkJoin, Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { MapDataService } from './map-data.service';
import { DEFAULT_TRIP_NAME, DutyData, GCPlannerData, MapItemType, MoonData, NoteData, TakeoutMileMarkers, Trip, TripConfig, TripStatus } from './interfaces';
import { DutyRosterComponent } from '../trip-config/duty-roster/duty-roster.component';

export enum StorageKeys {
  AppData = 'gcplanner',
  Duties = 'duties',
  Notes = 'notes',
  Moon = 'moon',
  ItemFilters = 'itemFilters',
  Selections = 'selections',
  TripConfig = 'tripConfig'
}

export enum PlannerDataItem {
  config,
  selections,
  notes,
  duties,
  filters,
  moon,
  trip
}

@Injectable({
  providedIn: 'root'
})
export class AppDataService implements OnDestroy {

  private static defaultGCPlannerData: GCPlannerData = {
    activeTripName: DEFAULT_TRIP_NAME,
    trips: [
      {
        dataVersion: 1,
        tripName: DEFAULT_TRIP_NAME,
        tripStatus: TripStatus.active,
        type: 'tripData',
        created: new Date(Date.now()),
        tripData: {
          tripConfig: {
            takeoutMileMarker: TakeoutMileMarkers.diamond,
            exchanges: false,
            hualapaiPermit: true,
            darkMode: true,
            configured: false
          },
          notes: [],
          selections: {I10: 'C'},
          duties: {} as DutyData,
          moon: {}
        }
      }
    ],
    itemFilters: [
      {
        class: 'allItems',
        code: '!',
        desc: 'Favorites',
        name: 'Favorites'
      }
    ],
    activeFavoritesSet: 'minimal',
    favoritesSets: [
      {
        name: 'minimal',
        items: ['I10', 'I6250', 'I7010']
      }
    ]
  };

  private svcSub: Subscription;

  constructor(private localDB: StorageMap,
              private mapDataService: MapDataService) {
    this.getAppData('appData Constructor').subscribe(appData => { console.log(`appData constructor data received: ${appData}.`); }).unsubscribe();
  }

  ngOnDestroy() {
    this.unsub();
  }
  private unsub() {
    if (this.svcSub && !this.svcSub.closed) {
      this.svcSub.unsubscribe();
    }
  }

  // This will create an empty data structure and save it.
  // If existing data from the previous version exists, it
  // will be pulled into the new structure.
  private initializeAppData(): GCPlannerData {
    console.log(`Initializing app data (because local storage hasn't been set yet)...`);

    const appData = AppDataService.defaultGCPlannerData;
    forkJoin([
      this.localDB.get(StorageKeys.TripConfig) as Observable<TripConfig>,
      this.localDB.get(StorageKeys.ItemFilters) as Observable<MapItemType[]>,
      this.localDB.get(StorageKeys.Notes) as Observable<any>,
      this.localDB.get(StorageKeys.Selections) as Observable<{}>,
//      this.localDB.get(StorageKeys.Duties) as Observable<string[]>
    ]).subscribe(existingData => {
      console.log(`App data retrieved: ${existingData}`);

      if (existingData[0] !== undefined) {
        appData.activeTripName = DEFAULT_TRIP_NAME;
        const trip = {
          dataVersion: 1,
          tripName: DEFAULT_TRIP_NAME,
          tripStatus: TripStatus.active,
          type: 'tripData',
          created: new Date(Date.now()),
          tripData: {
            tripConfig: this.upgradeConfig(existingData[0]),
            notes: [],
            selections: {},
            duties: {} as DutyData,
            moon: {},
            configured: true
          },
        } as Trip;
        if (existingData[1]) {
          appData.itemFilters = existingData[1];
        }
        if (existingData[2]) {
          trip.tripData.notes = this.upgradeNotes(existingData[2]);
        }
        if (existingData[3]) {
          trip.tripData.selections = existingData[3];
        }
        // if (existingData[4]) {
        //   trip.tripData.boaters = existingData[4];
        // }
        appData.trips.push(trip);
        // this.localDB.delete(StorageKeys.TripConfig).subscribe(() => {}).unsubscribe();
        // this.localDB.delete(StorageKeys.ItemFilters).subscribe(() => {}).unsubscribe();
        // this.localDB.delete(StorageKeys.Notes).subscribe(() => {}).unsubscribe();
        // this.localDB.delete(StorageKeys.Selections).subscribe(() => {}).unsubscribe();
        // this.localDB.delete(StorageKeys.Duties).subscribe(() => {}).unsubscribe();
      }
    }).unsubscribe();
    this.saveAppData(appData);
    return appData;
  }
  private upgradeNotes(data: any): NoteData[] {
    // if data is not found, return empty array.
    if (!data) {
      return [];
    }
    // if data is already an array, it's been
    // upgraded, so just return it.
    if (Array.isArray(data)) {
      return data;
    }

    // Here we have the old format which is just an object
    // representing a key/value array. We want additional
    // data, so we're going to convert it to an array.
    const noteData: NoteData[] = [];
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        const noteItem = {
          id: key,
          text: data[key],
          itemName: this.mapDataService.getItem(key).name,
          itemType: this.mapDataService.getItem(key).type,
          created: new Date(Date.now()),
          lastUpdate: new Date(Date.now())
        };
        noteData.push(noteItem);
      }
    }
    return noteData;
  }
  private upgradeConfig(data: any): TripConfig {
    if (!('configured' in data)) {
      const MILLISECONDS_PER_DAY = 86400000;
      const config: TripConfig = data;
      config.configured = true;
      config.days = config.maxDays;
      config.takeoutDate =
        new Date(config.launchDate.getTime() +
          ((config.days - 1) * MILLISECONDS_PER_DAY));
      data = config;
    }
    return data;
  }

  public clearAppData() {
    this.localDB.delete(StorageKeys.AppData).subscribe();
  }
  private saveAppData(appData: GCPlannerData) {
    this.localDB.set(StorageKeys.AppData, appData).subscribe().unsubscribe();
  }
  private getAppData(caller: string): Observable<GCPlannerData> {
    return (this.localDB.get(StorageKeys.AppData) as Observable<GCPlannerData>).pipe(
      map(appData => {
        if (appData === undefined) { // should only really happen on first load of the app.
          appData = this.initializeAppData();
        }
        return appData;
      })
    );
  }

  public getFilters(): Observable<MapItemType[]> {
    return this.getAppData('appdata getFilters').pipe(
      map(appData => {
        return appData.itemFilters;
      })
    );
  }
  public setFilters(newValue: MapItemType[]) {
    this.getAppData('appdata setFilters')
      .subscribe(appData => {
        appData.itemFilters = newValue;
        this.saveAppData(appData);
      });
  }

  public getTrip(): Observable<Trip> {
    return this.getAppData('appdata getTrip').pipe(
      map(appData => {
        return (appData.trips.find(t => t.tripName === appData.activeTripName));
      })
    );
  }
  public saveTrip(newServiceData: Trip) {
    this.getAppData('appData saveTrip')
      .subscribe(appData => {
        if (newServiceData.tripName?.length === 0) {
          newServiceData.tripName = DEFAULT_TRIP_NAME;
        }
        const tripIndex = appData.trips.findIndex(t => t.tripName === appData.activeTripName);
        appData.trips[tripIndex] = newServiceData;
        appData.activeTripName = newServiceData.tripName;
        this.saveAppData(appData);
      });
  }

  public getTripItem(itemType: StorageKeys): Observable<any> {
    return this.getAppData(`appdata getTripItem for ${itemType}`).pipe(
      map(appData => {
        const activeTrip = appData.trips.find(t => t.tripName === appData.activeTripName);
        switch (itemType) {
          case StorageKeys.TripConfig: {
            return activeTrip.tripData[itemType];
          }
          default: {
            return activeTrip.tripData[itemType] === undefined ? {} : activeTrip.tripData[itemType];
          }
        }
      })
    );
  }
  public saveTripItem(newValue: any, itemType: StorageKeys) {
    this.getAppData(`appdata saveTripItem for ${itemType}`)
      .subscribe(appData => {
        const activeTrip = appData.trips.find(t => t.tripName === appData.activeTripName);
        activeTrip.tripData[itemType] = newValue;
        this.saveAppData(appData);
      });
  }

  public setActiveTrip(tripName: string){
    this.getAppData('appdata setActiveTrip').subscribe(appData => {
      if (appData.trips.findIndex(t => t.tripName === tripName) === -1) {
        const errorMsg = `Trip ${tripName} doesn't exist and cannot be set as the active trip.`;
        console.error(errorMsg);
        throw new Error(errorMsg);
      } else {
        appData.activeTripName = tripName;
        this.saveAppData(appData);
      }
    });
  }
  public copyTrip(newName: string) {
    this.getAppData('appdata copyTrip')
      .subscribe(appData => {
        const activeTrip = appData.trips.find(t => t.tripName === appData.activeTripName);
        activeTrip.tripName = newName;
        appData.trips.push(activeTrip);
        appData.activeTripName = newName;
        this.saveAppData(appData);
      });
  }
}
