import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { TripConfigService } from '../../services/trip-config.service';
import { TakeoutMileMarkers, TripConfig } from '../../services/interfaces';
import { Network } from '@ngx-pwa/offline';
import { MoonService } from 'src/app/services/moon.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'gcp-config',
  templateUrl: './trip-config.component.html',
  styleUrls: ['./trip-config.component.scss']
})
export class TripConfigComponent implements OnInit, OnDestroy {

  @Output() darkModeToggled = new EventEmitter();
  constructor(private configService: TripConfigService,
              private moonService: MoonService,
              private network: Network) { }

  private MILLISECONDS_PER_DAY = 86400000;
  LAUNCH_DATE = 'LaunchDate';
  TAKEOUT = 'TakeoutMileMarker';
  DAYS = 'Days';

  online = this.network.online;
  tripConfig: TripConfig;
  prevTripConfig: TripConfig;
  takeoutMileMarkers = TakeoutMileMarkers; // for use in html
  private svcSub: Subscription;

  ngOnInit() {
    // load current values
    this.svcSub = this.configService.serviceData$.subscribe( data => {
      this.tripConfig = data;
      this.tripConfig.takeoutDate = this.getTakeoutDate();
      this.prevTripConfig = { ... this.tripConfig};
    });
  }

  ngOnDestroy() {
    this.unsub();
  }
  private unsub() {
    if (this.svcSub && !this.svcSub.closed) {
      this.svcSub.unsubscribe();
    }
  }

  apply(event: Event, attribute: string = '') {
    if (attribute === this.LAUNCH_DATE) {
      this.tripConfig.maxDays = this.configService.getMaxDays(this.tripConfig.launchDate, this.tripConfig.takeoutMileMarker);
      this.tripConfig.takeoutDate = this.getTakeoutDate();
      this.moonService.changeLaunchDate(this.tripConfig.launchDate);
    }
    if (attribute === this.TAKEOUT) {
      this.tripConfig.maxDays = this.configService.getMaxDays(this.tripConfig.launchDate, this.tripConfig.takeoutMileMarker);
    }
    if (attribute === this.DAYS) {
      if (this.tripConfig.days > this.tripConfig.maxDays) { return; }
      this.tripConfig.takeoutDate = this.getTakeoutDate();
    }
    // save the config
    this.configService.saveData(this.tripConfig);
    this.prevTripConfig = { ... this.tripConfig};
  }

  getTakeoutDate() {
    return new Date(this.tripConfig.launchDate.getTime() + ((this.tripConfig.days - 1) * this.MILLISECONDS_PER_DAY));
  }
}
