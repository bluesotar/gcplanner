import { Component, OnInit, OnDestroy } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { StepperOrientation } from '@angular/cdk/stepper';
import { map } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { DutyRosterService } from '../../services/duty-roster.service';
import { DutyData, DutyRosterItem } from '../../services/interfaces';
import { TripConfigService } from '../../services/trip-config.service';

@Component({
  selector: 'gcp-duty-roster',
  templateUrl: './duty-roster.component.html',
  styleUrls: ['./duty-roster.component.scss']
})
export class DutyRosterComponent implements OnInit, OnDestroy {

  dutyData: DutyData = {} as DutyData;
  svcSub: Subscription;
  dutiesColumns: string[] = ['active', 'duty', 'set'];
  teamColumns: string[] = ['boater', 'team', 'color'];
  assignmentColumns: string[] = ['day', 'set', 'team'];
  rosterColumns: string[] = ['set'];
  daysOnTheWater: number;
  teams: string[];
  sets: string[];

  stepperOrientation: Observable<StepperOrientation>;
  rosterData: DutyRosterItem[];

  constructor(private rosterService: DutyRosterService,
              private configService: TripConfigService,
              breakpointObserver: BreakpointObserver) {
    this.configService.serviceData$.subscribe(config => {
      this.daysOnTheWater = config.days;
      for (let d = 1; d <= this.daysOnTheWater; d++) {
        this.rosterColumns.push(`T${('0' + d).slice(-2)}`);
      }
    });
    this.stepperOrientation = breakpointObserver.observe('(min-width: 800px)')
      .pipe(map(({matches}) => matches ? 'horizontal' : 'vertical'));
  }

  ngOnInit() {
    this.svcSub = this.rosterService.serviceData$.subscribe( data => {
      this.dutyData = data;
      // console.log(`data change received roster = ${JSON.stringify(this.dutyData.roster)}`);

      if (this.dutyData.duties.length === 0 ||
          this.dutyData.duties.findIndex(d => d.duty.length === 0) === -1)
      {
        this.dutyData.duties.push({active: false, duty: '', set: '', description: ''});
      }

      if (this.dutyData.teams.length === 0 ||
          this.dutyData.teams.findIndex(t => t.boater.length === 0) === -1)
      {
        this.dutyData.teams.push({ boater: '', team: '', color: ''});
      }

      // if (this.dutyData.roster.length === 0 ||
      //     this.dutyData.roster.findIndex(r => r.set.length === 0) === -1)
      // {
      //   this.dutyData.roster.push({ set: '', teams: [] });
      // }

      this.sets = this.getSets();
      this.teams = this.getTeams();
    });
  }
  ngOnDestroy() {
    this.svcSub.unsubscribe();
  }

  generatePatternData() {
    // If the pattern is already set, just use it.
    if (this.dutyData.roster?.length > 0) { return; }

    // Otherwise, add a line for each dutyset for the first few days.
    // for (let s = 0; s < this.sets.length; s++) {
    this.sets.forEach(set => {
      if (set.length === 0) {
        return;
      }
      const rosterItem = {
        set,
        teams: {}
      };
      for (let x = 0; x < this.daysOnTheWater; x++) {
        const teamCol = `T${('0' + (x + 1)).slice(-2)}`;
        rosterItem.teams[teamCol] = '';
      }
      for (let t = 1; t <= this.teams.length; t++) {
        if (this.teams[t - 1].length === 0) {
          continue;
        }
//        console.log(`generating pattern data with s = ${set}, t = ${this.teams[t - 1]}, and sets = ${this.sets}`);
        const teamCol = `T${('0' + t).slice(-2)}`;
        rosterItem.teams[teamCol] = this.teams[t - 1];
      }
      this.dutyData.roster.push(rosterItem);
    });
    // console.log(`pattern generated: ${JSON.stringify(this.dutyData.roster)}`);
  }

  generateRosterData() {
    // Initialize with defined pattern
    this.rosterData = [];
    this.rosterData = [...this.dutyData.roster];

    // Repeat the pattern for the rest of the days on the river
    for (const rosterItem in this.rosterData) {
      if (rosterItem) {
        const ri = this.rosterData[rosterItem];
//        console.log(`rosterItem is ${JSON.stringify(ri)}, teams is ${JSON.stringify(ri.teams)}`);
        const patternSource = this.dutyData.roster[rosterItem].teams;

        for (let d = this.teams.length; d <= this.daysOnTheWater; ) {
          Object.keys(patternSource).forEach(key => {
            if (d <= this.daysOnTheWater) {
              const tcol = `T${('0' + d++).slice(-2)}`;
              ri.teams[tcol] = patternSource[key];
            }
          });
        }
      }
    }
  }
  getHeaderData(colIndex: number) {
    if (colIndex === 0) { return 'Set'; }
    return `${('0' + (colIndex)).slice(-2)}`;
  }
  getCellData(ri: DutyRosterItem, colIndex: number) {
    if (colIndex === 0) { return ri.set; }
    return ri.teams[`T${('0' + (colIndex)).slice(-2)}`];
  }
  getTeams() {
    return this.dutyData.teams ? [...new Set(this.dutyData.teams.map(t => t.team))] : [];
  }
  getSets() {
    return this.dutyData.duties ? [...new Set(this.dutyData.duties.map(d => d.set))] : [];
  }

  // apply() {
  //   this.rosterService.saveData(this.dutyData);
  // }
  moveTo(nextPage: string) {
    if (nextPage === 'teams') {
      // console.log(`moving to teams`);
      if (this.dutyData.teams.findIndex(t => t.boater.length === 0) === -1) {
        this.dutyData.teams.push({ boater: '', team: '', color: ''});
      }
      }
    if (nextPage === 'pattern') {
      // console.log(`moving to pattern`);
      this.generatePatternData();
    }
    if (nextPage === 'roster') {
      // console.log(`moving to roster`);
      this.generateRosterData();
    }
  }
  setDuty() {
    this.dutyData.duties = this.dutyData.duties.filter(
      dutyItem => dutyItem.active === true &&
      dutyItem.duty.length > 0 &&
      dutyItem.set.length > 0);
    this.rosterService.saveData(this.dutyData);
    if (this.dutyData.duties.findIndex(d => d.duty.length === 0) === -1) {
      this.dutyData.duties.push({active: false, duty: '', set: '', description: ''});
    }
    // this.generatePatternData();
    // this.generateRosterData();
}
  setTeam() {
    this.dutyData.teams = this.dutyData.teams.filter(team => team.boater.length > 0 || team.team.length > 0);
//    console.log(`updating teams to ${JSON.stringify(this.dutyData.teams)} (after filter)`);
    this.rosterService.saveData(this.dutyData);
    if (this.dutyData.teams.findIndex(t => t.boater.length === 0) === -1) {
      this.dutyData.teams.push({ boater: '', team: '', color: ''});
    }
    // console.log(`team changed, regenerating dutydata from ${JSON.stringify(this.dutyData.roster)}`);
    // this.generatePatternData();
    // console.log(`to                                       ${JSON.stringify(this.dutyData.roster)}`);
    // this.generateRosterData();
}
  setPatternItem() {
    // console.log(`Pattern changed ${JSON.stringify(this.dutyData.roster)}`);
    this.dutyData.roster = this.dutyData.roster.filter(rosterItem => rosterItem.set.length > 0);
    this.rosterService.saveData(this.dutyData);
    // if (this.dutyData.roster.findIndex(r => r.set.length === 0) === -1) {
    //   this.dutyData.roster.push({ set: '', teams: [] });
    // }
    // this.generateRosterData();
  }
}
