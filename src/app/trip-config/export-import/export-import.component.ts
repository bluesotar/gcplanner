import { Component, OnInit } from '@angular/core';
import { TripConfigService } from '../../services/trip-config.service';
import { NoteService } from '../../services/note.service';
import { SelectionsService } from '../../services/selections.service';
import { DutyRosterService } from '../../services/duty-roster.service';
import { ThemePalette } from '@angular/material/core';
import { AppDataService, StorageKeys } from '../../services/app-data.service';

interface Task {
  name: string;
  selected: boolean;
  color: ThemePalette;
  subtasks?: Task[];
  svcData?: string;
  key: string;
}

@Component({
  selector: 'gcp-export-import',
  templateUrl: './export-import.component.html',
  styleUrls: ['./export-import.component.scss']
})
export class ExportImportComponent implements OnInit {
  data: string;

  options: Task[] =  [
    { name: 'Trip Configuration', selected: false, color: 'accent', key: StorageKeys.TripConfig },
    { name: 'Destinations', selected: false, color: 'accent', key: StorageKeys.Selections },
    { name: 'Notes', selected: false, color: 'accent', key: StorageKeys.Notes },
    { name: 'Duty Roster', selected: false, color: 'accent', key: StorageKeys.Duties }
  ];


  constructor(private noteService: NoteService,
              private tripConfigService: TripConfigService,
              private selectionsService: SelectionsService,
              private dutyRosterService: DutyRosterService,
              private appDataService: AppDataService) {
    tripConfigService.serviceData$.subscribe(data => {
      this.options[0].svcData = `"${StorageKeys.TripConfig}": ${JSON.stringify(data, null, '\t')}`;
    });
    selectionsService.serviceData$.subscribe(data => {
      this.options[1].svcData = `"${StorageKeys.Selections}": ${JSON.stringify(data, null, '\t')}`;
    });
    noteService.serviceData$.subscribe(data => {
      this.options[2].svcData = `"${StorageKeys.Notes}": ${JSON.stringify(data, null, '\t')}`;
    });
    dutyRosterService.serviceData$.subscribe(data => {
      this.options[3].svcData = `"${StorageKeys.Duties}": ${JSON.stringify(data, null, '\t')}`;
    });
}

  public ngOnInit() {
    this.load();
  }

  public reset() {
    this.appDataService.clearAppData(); // This should probably trigger updates to subscriptions.
  }
  public copy() {
    navigator.clipboard.writeText(this.data);
  }
  public load() {
    let commaNewline = '\t';
    this.data = '{\n';

    this.options.forEach( opt => {
      if (opt.selected) {
        this.data += `${commaNewline}${opt.svcData} \n`;
        commaNewline = ',\n\t';
      }
    });
    this.data += '\n}';
  }

  public save() {
    this.data = JSON.parse(this.data);
    this.options.forEach( t => {
      if (this.data.hasOwnProperty(t.key)) {
        switch (t.key) {
          case StorageKeys.TripConfig: {
            // convert input from old version data.
            if (!('configured' in this.data[t.key])) {
              const MILLISECONDS_PER_DAY = 86400000;
              const config = this.data[t.key];
              config.configured = true;
              config.days = config.maxDays;
              config.launchDate = (typeof config.launchDate === 'string') ? new Date(config.launchDate) : config.launchDate as Date;
              config.takeoutDate =
                new Date(config.launchDate.getTime() +
                  ((config.days - 1) * MILLISECONDS_PER_DAY));
              this.data[t.key] = config;
            }
            this.tripConfigService.saveData(this.data[t.key]);
            break;
          }
          case StorageKeys.Selections: {
            this.selectionsService.saveData(this.data[t.key]);
            break;
          }
          case StorageKeys.Notes: {
            this.data[t.key] = this.noteService.upgrade(this.data[t.key]);
            this.noteService.saveData(this.data[t.key]);
            break;
          }
          case StorageKeys.Duties: {
            this.dutyRosterService.saveData(this.data[t.key]);
            break;
          }
          default: {
            break;
          }
        }
      }
    });

    this.load();
  }
}
