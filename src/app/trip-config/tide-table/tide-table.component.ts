import { Component, OnInit, OnDestroy } from '@angular/core';
import { TideTablesService } from '../../services/tide-tables.service';
import { TripConfigService } from '../../services/trip-config.service';
import { Observable, Subscription } from 'rxjs';
import { TidePlace } from '../../services/interfaces';

@Component({
  selector: 'gcp-tide-table',
  templateUrl: './tide-table.component.html',
  styleUrls: ['./tide-table.component.scss']
})
export class TideTableComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['mile', 'place', 'delay40', 'rise40', 'delay43', 'rise43', 'delay45', 'rise45', 'delay50', 'rise50'];
  private svcSub: Subscription;
  tides: Observable<TidePlace[]>;
  listItems: TidePlace[] = [];
  filterValue = '';

  cfs: number;
  constructor(private tideTablesService: TideTablesService,
              private tripConfigService: TripConfigService) { }

  ngOnInit() {
    this.tides = this.tideTablesService.serviceData$;
    this.svcSub = this.tripConfigService.serviceData$.subscribe(config => this.cfs = config.cfs);
    this.loadList();
  }
  ngOnDestroy() {
    if (this.svcSub && !this.svcSub.closed) {
      this.svcSub.unsubscribe();
    }
  }
  applyFilter(event: Event) {
    this.loadList();
  }
  private loadList() {
    this.listItems = [];

    // Load Selections
    this.tides.subscribe( tides => {
      tides.forEach(item => {
        if ( this.filterValue.length === 0 || item.place.toLowerCase().search(this.filterValue.toLowerCase()) >= 0 ) {
          this.listItems.push(item);
        }
      });
    }).unsubscribe();
  }

}
