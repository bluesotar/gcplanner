import { Component } from '@angular/core';
import { MatSnackBarRef } from '@angular/material/snack-bar';

@Component({
  selector: 'gcp-ios-install',
  templateUrl: './ios-install.component.html',
  styleUrls: ['./ios-install.component.scss']
})
export class IosInstallComponent {

  constructor( private snackBarRef: MatSnackBarRef<IosInstallComponent> ) { }

  close() {
    this.snackBarRef.dismiss();
  }
}
