import {MediaMatcher} from '@angular/cdk/layout';
import {ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import { Subscription } from 'rxjs';
import { TripConfigService } from '../../services/trip-config.service';
import { SelectionsService } from '../../services/selections.service';
import { TripConfig } from '../../services/interfaces';

@Component({
  selector: 'gcp-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit, OnDestroy {
  navOptions = [
    { route: '/intro', text: 'Introduction', title: 'Introduction' },
    { route: '/config', text: 'Settings', title: 'Settings' },
    { route: '/chooser', text: 'Destinations', title: 'Choose Destinations' },
    { route: '/itinerary', text: 'Itinerary', title: 'Itinerary' },
    { route: '/tide-table', text: 'Tide Table', title: 'Tide Table' },
    { route: '/duty-roster', text: 'Duty Roster', title: 'Duty Roster' },
    { route: '/trip-log', text: 'Trip Log', title: 'Trip Log', icon: 'mode_comment' },
    { route: '/export-import', text: 'Share', title: 'Share Settings' },
    { route: '/about', text: 'About', title: 'About GC Planner'}
  ];
  bottomNavOptions = [
    { route: '/chooser', text: 'Destinations', title: 'Choose Destinations',
              icon: 'check_circle_outline', badgeValue: 0, badgeColor: 'accent' },
    { route: '/itinerary', text: 'Itinerary', title: 'Itinerary', icon: 'list' },
    { route: '/duty-roster', text: 'Duty Roster', title: 'Duty Roster', icon: 'grid_on' },
    { route: '/trip-log', text: 'Trip Log', title: 'Trip Log', icon: 'mode_comment' },
    { route: '/tide-table', text: 'Tide Table', title: 'Tide Table', icon: 'water' },
  ];
  topNavOptions = [
    { route: '/export-import', text: 'Share', title: 'Share', icon: 'ios_share' },
    { route: '/config', text: 'Settings', title: 'Settings', icon: 'settings' },
  ];
  mobileQuery: MediaQueryList;
  private mobileQueryListener: () => void;
  public title = 'Canyon Planner';
  svcSub: Subscription[] = [];
  tripConfig: TripConfig;

  constructor(changeDetectorRef: ChangeDetectorRef,
              media: MediaMatcher,
              private selectionsService: SelectionsService,
              private tripConfigService: TripConfigService ) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener ?
        this.mobileQuery.addEventListener('change', this.mobileQueryListener) :
        this.mobileQuery.addListener(this.mobileQueryListener);
  }
  ngOnInit(): void {
    this.svcSub.push(
      this.tripConfigService.serviceData$.subscribe(config => {
        this.tripConfig = config;
        this.svcSub.push(this.selectionsService.campCount$.subscribe(count => {
          this.bottomNavOptions[0].badgeValue = count; // Destinations icon, nights used.
          this.bottomNavOptions[0].badgeColor = count > this.tripConfig?.maxDays ? 'warn' : 'accent';
        }));
      })
    );

  }
  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener ?
        this.mobileQuery.removeEventListener('change', this.mobileQueryListener) :
        this.mobileQuery.removeListener(this.mobileQueryListener);
    this.svcSub.forEach(s => this.unsub(s));
  }
  private unsub(sub: Subscription) {
    if (sub && !sub.closed) {
      sub.unsubscribe();
    }
  }

  setTitle(newTitle: string) {
    this.title = newTitle;
  }
}
