import { Component } from '@angular/core';
import { Network } from '@ngx-pwa/offline';

@Component({
  selector: 'gcp-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent {
  online = this.network.online;

  constructor(protected network: Network) {
    console.log(`Online state is ${this.online}.`);
  }
  public refresh() {
    window.location.reload();
  }
}
