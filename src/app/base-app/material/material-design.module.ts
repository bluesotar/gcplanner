import { NgModule } from '@angular/core';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule} from '@angular/material/form-field';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { LayoutModule } from '@angular/cdk/layout';
import { MatTableModule } from '@angular/material/table';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatStepperModule } from '@angular/material/stepper';

const modules = [
  MatButtonModule, MatCheckboxModule, MatSelectModule,
  MatSliderModule, MatDatepickerModule, MatNativeDateModule,
  MatFormFieldModule, MatInputModule, LayoutModule, MatToolbarModule,
  MatSidenavModule, MatIconModule, MatListModule, MatCardModule,
  MatRadioModule, MatSnackBarModule, MatSnackBarModule, MatTableModule,
  MatSlideToggleModule, MatBadgeModule, MatDialogModule, MatStepperModule
];
@NgModule({
  imports: [...modules],
  exports: [
    ...modules
  ]
})
export class MaterialDesignModule { }
