import { Component, EventEmitter,
         Output, OnInit, OnDestroy } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { IosInstallComponent } from './base-app/ios-install/ios-install.component';
import { TripConfigService } from './services/trip-config.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'gcp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  private svcSub: Subscription;

  title = 'Grand Canyon Planner';
  public theme = 'canyon-dark-theme';
  @Output() toggleSideNav = new EventEmitter();
  constructor(private toast: MatSnackBar,
              private tripConfigService: TripConfigService) {
  }

  onDarkModeToggled(mode) {
    if (mode) {
      this.theme = `canyon-dark-theme`;
    } else {
      this.theme = `canyon-light-theme`;
    }
  }
  ngOnInit() {
    this.svcSub = this.tripConfigService.serviceData$.subscribe(config => {
      this.onDarkModeToggled(config.darkMode);
    });

    // Detects if device is on iOS
    const isIos = () => {
      const userAgent = window.navigator.userAgent.toLowerCase();
      return /iphone|ipad|ipod/.test( userAgent );
    };
    // Detects if device is in standalone mode
    const isInStandaloneMode = () => ('standalone' in (window as any).navigator) && ((window as any).navigator.standalone);

    // Checks if should display install popup notification:
    if (isIos() && !isInStandaloneMode()) {
      this.toast.openFromComponent(IosInstallComponent, {
        duration: 8000,
        horizontalPosition: 'start',
        panelClass: ['mat-elevation-z3']
      });
    }
  }
  ngOnDestroy() {
    if (this.svcSub && !this.svcSub.closed) {
      this.svcSub.unsubscribe();
    }
  }
}
