import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { NoteService } from '../../services/note.service';
import { Subscription } from 'rxjs';
import { MapDataService } from '../../services/map-data.service';
import { NoteData } from '../../services/interfaces';

@Component({
  selector: 'gcp-trip-log',
  templateUrl: './trip-log.component.html',
  styleUrls: ['./trip-log.component.scss']
})
export class TripLogComponent implements OnInit, OnDestroy {

  @ViewChild('notes', { static: true }) textarea: ElementRef;
  id: string;
  noteData: NoteData;
  svcSub: Subscription;

  constructor(private noteService: NoteService,
              private mapDataService: MapDataService) {
    this.id = 'I00';
  }

  ngOnInit() {
    this.noteData = this.noteService.getItem(this.id);
  }
  ngOnDestroy() {
    this.unsub();
  }
  private unsub() {
    if (this.svcSub && !this.svcSub.closed) {
      this.svcSub.unsubscribe();
    }
  }

  done() {
    this.noteService.set(this.id, this.noteData.text);
  }
  addTS() {
    const caretPos = this.textarea.nativeElement.selectionStart;
    const textAreaTxt = this.textarea.nativeElement.value;
    this.textarea.nativeElement.value = textAreaTxt.substring(0, caretPos) + this.getTS() + textAreaTxt.substring(caretPos);
    this.noteData.text = this.textarea.nativeElement.value;
    this.textarea.nativeElement.setSelectionRange(this.noteData.text.length, this.noteData.text.length);
    this.textarea.nativeElement.focus();
  }

  private getPart = (datePart: number) => datePart <= 9 ? `0${datePart}` : `${datePart}`;

  getTS(): string {
    const date = new Date(Date.now());
    const mo = this.getPart(date.getMonth() + 1);
    const dy = this.getPart(date.getDate());
    const hr = this.getPart(date.getHours());
    const mm = this.getPart(date.getMinutes());
    return ` ${date.getFullYear()}-${mo}-${dy} ${hr}:${mm} `;
  }
}
