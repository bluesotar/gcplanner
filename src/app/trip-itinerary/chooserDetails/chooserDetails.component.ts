import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { MapDataService } from '../../services/map-data.service';
import { Subscription } from 'rxjs';
import { ItemFiltersService } from '../../services/item-filters.service';
import { ChooserComponent } from '../chooser/chooser.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MapItem, ChooserDetailsData } from '../../services/interfaces';

@Component({
  selector: 'gcp-chooser-details',
  templateUrl: './chooserDetails.component.html',
  styleUrls: ['./chooserDetails.component.scss']
})
export class ChooserDetailsComponent implements OnInit, OnDestroy {
  id: string;
  mapItem: MapItem;
  return: string;
  typeName: string;
  svcSub: Subscription;
  typeOptions = ItemFiltersService.MapItemTypes;

  constructor(private mapDataService: MapDataService,
              public dialogRef: MatDialogRef<ChooserComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ChooserDetailsData) { }

  ngOnInit() {
    this.id = this.data.id;
    this.mapItem = this.mapDataService.getItem(this.id);
    this.typeName = this.typeOptions.find(to => to.code === this.mapItem.type ).name;
  }
  ngOnDestroy() {
    this.unsub();
  }
  private unsub() {
    if (this.svcSub && !this.svcSub.closed) {
      this.svcSub.unsubscribe();
    }
  }

  close() {
    this.dialogRef.close();
  }
}
