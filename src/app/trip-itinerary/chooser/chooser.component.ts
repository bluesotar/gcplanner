import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MapDataService } from '../../services/map-data.service';
import { TripConfigService } from '../../services/trip-config.service';
import { ItemFiltersService } from '../../services/item-filters.service';
import { SelectionsService } from '../../services/selections.service';
import { Router, NavigationEnd } from '@angular/router';
import { NoteService } from '../../services/note.service';
import { SunDataService } from '../../services/sun-data.service';
import { Subscription, Subject } from 'rxjs';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotesComponent } from '../notes/notes.component';
import { MapItem, MapItemType, TripConfig } from '../../services/interfaces';


interface ChooserListItem {
  mapItem: MapItem;
  action: string;
  class?: string;
  sunrise?: string;
  sunset?: string;
  allowCamp: boolean;
  allowVisit: boolean;
  sideClass?: string;
  mileage?: number;
}

@Component({
  selector: 'gcp-chooser-filter',
  templateUrl: 'chooserFilterDialog.component.html'
})
export class ChooserFilterDialogComponent {
  public filterValue: string;
  constructor(
    public dialogRef: MatDialogRef<ChooserFilterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {filterValue: string}) {
    this.filterValue = data.filterValue;
    this.dialogRef.backdropClick().subscribe(() => { dialogRef.close(this.filterValue); });
  }
}

@Component({
  selector: 'gcp-chooser',
  templateUrl: './chooser.component.html',
  styleUrls: ['./chooser.component.scss'],
  providers: [ItemFiltersService]
})
export class ChooserComponent implements OnInit, OnDestroy {
  displayedColumns: string[] = ['distance', 'mile', 'name', 'notes'];

  types = new FormControl();
  previousTypesValue: MapItemType[] = [];
  typeOptions = ItemFiltersService.MapItemTypes;
  listItems: ChooserListItem[] = [];
  filterValue = '';
  maxTripDays: number;
  problemOnPage = '';
  tripConfig: TripConfig;
  mapData: MapItem[];
  public EXCHANGECAMP = 'E';
  public TRIBAL = 'H';
  public POI = 'P';
  private svcSub: Subscription[] = [];
  private lastMile = 0;
  public includeSun = true;
  private dataLoaded = new Subject();
  private allDataLoaded = 0;

  constructor(private router: Router,
              public dialog: MatDialog,
              private tripConfigService: TripConfigService,
              private itemFiltersService: ItemFiltersService,
              private selectionsService: SelectionsService,
              private noteService: NoteService,
              private sunDataService: SunDataService,
              private mapDataService: MapDataService) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
    this.svcSub.push(this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing its last link wasn't previously loaded
        this.router.navigated = false;
      }
    }));
  }
  applyFilter(event: Event) {
    this.loadList();
  }

  public updateCb(event: any, i: number, value: string) {
    this.listItems[i].action = event.target.checked ? value : '';
    this.selectionsService.set(this.listItems[i].mapItem.id, this.listItems[i].action);
  }
  public setFromMile(event: any, mile: number) {
    this.lastMile = mile;
    this.listItems.forEach(i => {
      if (mile > 0 && i.mapItem.mile > mile) {
        i.mileage = i.mapItem.mile - mile;
      } else {
        i.mileage = 0;
      }
    });
  }
  public selectItem(event: any, i: number) {
    const CAMP = 'C';
    const LAYOVER = 'L';
    const NOACTION = '';
    const STOP = 'S';
    const NOTE = 'N';
    let newAction = NOACTION;
    if ( this.listItems[i].mapItem.type === CAMP ) {
      if (this.listItems[i].allowCamp) {
        switch (this.listItems[i].action) {
          case '':
            newAction = CAMP;
            break;
          case CAMP:
            newAction = LAYOVER;
            break;
          default:
            newAction = NOACTION;
            break;
        }
      }
    } else {
      newAction = this.listItems[i].action === NOACTION ? STOP : NOACTION;
    }
    this.listItems[i].action = newAction;
    this.selectionsService.set(this.listItems[i].mapItem.id, newAction);
  }
  public setFilter() {
    const prevHasAll = this.previousTypesValue.findIndex(t => t.code === '*') >= 0;
    const currHasAll = this.types.value.findIndex(t => t.code === '*') >= 0;
    const prevHasFavs = this.previousTypesValue.findIndex(t => t.code === '!') >= 0;
    const currHasFavs = this.types.value.findIndex(t => t.code === '!') >= 0;

    // if the last item has been unchecked, set a default value
    if (this.types.value.length === 0) {
      if (prevHasAll || !prevHasFavs) {
        this.types.setValue(ItemFiltersService.MapItemTypes.filter(tp => tp.code === '!'));
      } else {
        this.types.setValue(ItemFiltersService.MapItemTypes.filter(tp => tp.code === '*'));
      }
    } else if (this.types.value.length > 1) {
      // Handle the addition/removal of all/fav to existing list
      if (prevHasAll) {
        this.types.setValue(this.types.value.filter(tp => tp.code !== '*'));
      } else if (currHasAll) {
        this.types.setValue(this.types.value.filter(tp => tp.code === '*'));
      } // else leave types alone.
      if (prevHasFavs) {
        this.types.setValue(this.types.value.filter(tp => tp.code !== '!'));
      } else if (currHasFavs) {
        this.types.setValue(this.types.value.filter(tp => tp.code === '!'));
      } // else leave types alone
    } else { // types.len === 1
      // There's no way to activate a single item without an intervening 0 or > 1
    }
    this.previousTypesValue = this.types.value;
    this.itemFiltersService.saveData(this.types.value);

    // If camps are included, show sunshine.
    this.setDisplayedColumns();

    // Filters have changed, so reload items
    this.loadList();
  }
  // If camps are included, show sunshine.
  private setDisplayedColumns(){
    if ( this.types.value.findIndex(tp => tp.code === 'C' || tp.code === '*') >= 0)  {
      this.includeSun = true;
      this.displayedColumns = ['distance', 'mile', 'name', 'sunshine', 'notes'];
    } else {
      this.includeSun = false;
      this.displayedColumns = ['distance', 'mile', 'name', 'notes'];
    }
  }

  // Show details of the selected item
  public goBeta(id: string, name: string, index: number): void {
    // console.log(`Going to notes for ${id}, ${name}, ${index}`);

    const dialogRef = this.dialog.open(NotesComponent, {
      data: { id, name }
    });

    dialogRef.afterClosed().subscribe((noteText: string) => {
      // update the selections list. if the item isn't there, add it
      if (noteText.length > 0) {
        if (this.listItems[index].action === '') {
          this.listItems[index].action = 'N';
          this.selectionsService.set(this.listItems[index].mapItem.id, 'N');
          // console.log(`Adding item ${this.listItems[index].mapItem.id} for a note`);
        }
      } else {
      // If it is there, but only for the note which
      // is now gone, delete it.
      if (this.listItems[index].action === 'N') {
          this.listItems[index].action = '';
          this.selectionsService.set(this.listItems[index].mapItem.id, '');
          // console.log(`Removing item ${this.listItems[index].mapItem.id} for a note`);
        }
      }
    });
  }
  // Filter list
  public search(): void {
    if (this.filterValue.length === 0) {
      const currentFilter = this.filterValue;
      const dialogRef = this.dialog.open(ChooserFilterDialogComponent, {
        data: { filterValue: currentFilter }
      });

      dialogRef.afterClosed().subscribe(newFilter => {
        this.filterValue = newFilter;
        this.applyFilter(null);
      });
    } else {
      this.filterValue = '';
      this.applyFilter(null);
    }
  }

  ngOnInit() {
    const dataLoadingServices = 3;
    const dataServicesLoaded = {
      filterTypes: 0,
      mapData: 0,
      config: 0
    };
    this.dataLoaded.subscribe((dataId: string) => {
//      console.log(`chooser::ngOnInit with allDataLoaded = ${this.allDataLoaded} and dataId = ${dataId} and ${dataServicesLoaded[dataId]}`);
      dataServicesLoaded[dataId]++;
      if (dataServicesLoaded[dataId] === 1 && ++this.allDataLoaded >= dataLoadingServices) {
        this.loadList();
      }
    });
    this.svcSub.push(this.itemFiltersService.serviceData$.subscribe(data => {
//      console.log(`filters received with a count of ${data.length}`);
      if (data.length > 0) {
        this.previousTypesValue = data as MapItemType[];

        this.types = new FormControl();
        const staticTypes = ItemFiltersService.MapItemTypes.filter(tp => this.previousTypesValue.findIndex(ptp => tp.code === ptp.code) >= 0);
        this.types.setValue(staticTypes);
        this.setDisplayedColumns();
        this.dataLoaded.next('filterTypes');
      }
    }));
    this.svcSub.push(this.mapDataService.serviceData$.subscribe(md => {
//      console.log(`mapData received with a count of ${md.length}`);
      if (md && md.length > 0) {
        this.mapData = md;
        this.dataLoaded.next('mapData');
      }
    }));
    this.svcSub.push(this.tripConfigService.serviceData$.subscribe(config => {
//      console.log(`configuration received with configured = ${config.configured} and maxDays = ${config.maxDays}`);
      if (config && config.configured) {
        this.tripConfig = config;
        this.maxTripDays = this.tripConfig.maxDays;
        this.dataLoaded.next('config');
      }
    }));
    this.svcSub.push(this.noteService.serviceData$.subscribe(notesData => {

      this.listItems.forEach(li => {
        li.class = li.class.replace(' hasNote', ''); // Default to no notes.
        // if (li.mapItem.id === 'I890') {
        //   console.log(`item ${li.mapItem.id} has class ${li.class}`);
        // }
        if (notesData.findIndex(nd => nd.id === li.mapItem.id) >= 0) {
          li.class += ' hasNote';
        }
        // if (li.mapItem.id === 'I890') {
        //     console.log(`item ${li.mapItem.id} now has class ${li.class}`);
        // }
      });
    }));
  }
  ngOnDestroy() {
    this.svcSub.forEach(s => this.unsub(s));
  }
  private unsub(sub: Subscription) {
    if (sub && !sub.closed) {
      sub.unsubscribe();
    }
  }

  getClass(mapItem: MapItem) {
    let className = '';
    const mapItemType = ItemFiltersService.MapItemTypes.find( t => t.code === mapItem.type);
    if (mapItemType !== undefined) {
      className = mapItemType.class;
      if (this.noteService.hasNote(mapItem.id)) { className += ' hasNote'; }
    }
    return className;
  }
  getSideClass(mapItem: MapItem) {
    let className = '';
    switch (mapItem.side) {
      case 'L':
        className += ' river-left';
        break;
      case 'R':
        className += ' river-right';
        break;
      default:
        className += ' river-center';
        break;
    }
    return className;
  }

  public loadList() {
    // console.log(`chooser: initializing this.listItems`);
    this.listItems = [];
    if (!this.tripConfig.configured) {
      return;
    }
    const date = new Date(this.tripConfig.launchDate.toISOString());
    const LEFT = 'L';
    const CAMP_TYPE = 'C';

    const includeAll = this.types.value[0].code === '*';
    const onlyFavorites = this.types.value[0].code === '!';

    // Load Selections
    this.mapData
      // Apply filters first
      .filter(item => item.mile <= this.tripConfig.takeoutMileMarker &&
        ( includeAll ||
          this.types.value.findIndex(filterType => filterType.code === item.type ) >= 0 ||
          (onlyFavorites && item.subTypes.includes('P'))) &&
        ( this.filterValue.length === 0 || item.name.toLowerCase().search(this.filterValue.toLowerCase()) >= 0 )
      )
      .forEach(item => {
        const listItem: ChooserListItem = {
          mapItem: item,
          action: '',
          class: '',
          allowCamp: true,
          allowVisit: true,
          sideClass: ''
        };

        // No stops allowed on river left without permit
        if (this.tripConfig.hualapaiPermit === false
              && item.side === LEFT
              && item.mile > MapDataService.HUALAPAI_LAND_START
              && item.mile < MapDataService.HUALAPAI_LAND_END) {
          listItem.allowVisit = false;
          listItem.allowCamp = false;
        }
        // Update item with action.
        listItem.action = this.selectionsService.getItem(item.id);
        // Update item with notes indicator
        listItem.class = this.getClass(listItem.mapItem);
        listItem.sideClass = this.getSideClass(listItem.mapItem);
        listItem.mileage = listItem.mapItem.mile - this.lastMile;
        // Additional settings for camps
        if (listItem.mapItem.type === CAMP_TYPE) {
          // Determine if there is any sunshine data associated with the camp
          const pd = this.sunDataService.getSunny(date.getMonth(), item.mile);
          if (pd !== undefined) {
            listItem.sunrise = pd.sunrise;
            listItem.sunset = pd.sunset;
          }
          // No camping in exchange camps if not exchanging.
          if (this.tripConfig.exchanges === false
              && item.subTypes[1] === this.EXCHANGECAMP) {
            listItem.allowCamp = false;
          }
        }
        this.listItems.push(listItem);
      });
  }
}
