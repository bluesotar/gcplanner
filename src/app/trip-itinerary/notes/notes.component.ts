import { Component, OnInit, Inject, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { NoteService } from '../../services/note.service';
import { Subscription } from 'rxjs';
import { ItemFiltersService } from '../../services/item-filters.service';
import { MapDataService } from '../../services/map-data.service';
import { ItineraryComponent } from '../itinerary/itinerary.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ItineraryDetailsData, MapItem, NoteData } from '../../services/interfaces';

@Component({
  selector: 'gcp-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit, OnDestroy {

  @ViewChild('notes', { static: true }) textarea: ElementRef;
  id: string;
  mapItem: MapItem;
  noteData: NoteData;
  return: string;
  typeName: string;
  itemName: string;
  svcSub: Subscription;
  typeOptions = ItemFiltersService.MapItemTypes;
  templates = {
    C: `Setup difficulty:
Shelter:
Arrived at:
Departed at:
Comments: `,
    R: `Ease of scout:
Arrived at:
Departed at:`,
    H: `Difficulty:
Arrived at:
Departed at:`,
    F: `Distance from river:`
  };

  constructor(private noteService: NoteService,
              private mapDataService: MapDataService,
              public dialogRef: MatDialogRef<ItineraryComponent>,
              @Inject(MAT_DIALOG_DATA) public data: ItineraryDetailsData) { }

  ngOnInit() {
    this.id = this.data.id;
    this.noteData = this.noteService.getItem(this.id);
    this.mapItem = this.mapDataService.getItem(this.id);
    this.typeName = this.typeOptions.find(to => to.code === this.mapItem.type ).name;
    if (this.noteData.text.length === 0) {
      if (this.mapItem.type in this.templates) {
        this.noteData.text = this.templates[this.mapItem.type];
      }
    }
    this.dialogRef.backdropClick().subscribe(() => { this.done(); });
  }
  ngOnDestroy() {
    this.unsub();
  }
  private unsub() {
    if (this.svcSub && !this.svcSub.closed) {
      this.svcSub.unsubscribe();
    }
  }

  done() {
    this.noteService.set(this.id, this.noteData.text);
    this.close();
  }
  addTS() {
    const caretPos = this.textarea.nativeElement.selectionStart;
    const textAreaTxt = this.textarea.nativeElement.value;
    this.textarea.nativeElement.value = textAreaTxt.substring(0, caretPos) + this.getTS() + textAreaTxt.substring(caretPos);
    this.noteData.text = this.textarea.nativeElement.value;
  }

  getTS(): string {
    const date = new Date(Date.now());
    const mo = this.getPart(date.getMonth() + 1);
    const dy = this.getPart(date.getDate());
    const hr = this.getPart(date.getHours());
    const mm = this.getPart(date.getMinutes());
    return ` ${date.getFullYear()}-${mo}-${dy} ${hr}:${mm} `;
  }
  private getPart(dp: number): string {
    return dp <= 9 ? `0${dp}` : `${dp}`;
  }
  clear() {
    this.noteData.text = '';
    this.done();
  }
  close() {
    this.dialogRef.close(this.noteData.text);
  }
}
