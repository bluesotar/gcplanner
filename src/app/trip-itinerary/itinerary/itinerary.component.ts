import { Component, OnInit, OnDestroy } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Network } from '@ngx-pwa/offline';
import { MapDataService } from '../../services/map-data.service';
import { TripConfigService } from '../../services/trip-config.service';
import { ItemFiltersService } from '../../services/item-filters.service';
import { SelectionsService } from '../../services/selections.service';
import { MoonService } from '../../services/moon.service';
import { TideTablesService } from '../../services/tide-tables.service';
import { NoteService } from '../../services/note.service';
import { SunDataService } from '../../services/sun-data.service';
import { Subscription, Subject } from 'rxjs';
import { NotesComponent } from '../notes/notes.component';
import { MatDialog } from '@angular/material/dialog';
import { MapItem, MoonData, TidePlace, TripConfig } from '../../services/interfaces';
import { ChooserFilterDialogComponent } from '../chooser/chooser.component';

interface ItineraryListItem {
  mapItem: MapItem;
  action: string;
  actionName: string;
  sunrise?: string;
  sunset?: string;
  moonImage?: string;
  tideRises?: string;
  day?: number;
  dateStr?: string;
  date?: Date;
  distance?: number;
  sunNotes?: string;
  class?: string;
  sideClass?: string;
}

@Component({
  selector: 'gcp-itinerary',
  templateUrl: './itinerary.component.html',
  styleUrls: ['./itinerary.component.scss']
})
export class ItineraryComponent implements OnInit, OnDestroy {
  static CAMP = 'C';
  EXCHANGECAMP = 'E';
  TRIBAL = 'H';

  displayedColumns: string[] = ['miles', 'place', 'moon', 'notes'];
  tides: TidePlace[];
  tripConfig: TripConfig;
  mapData: MapItem[];
  moonData: {};
  listItems: ItineraryListItem[] = [];
  svcSub: Subscription[] = [];
  private actions: {};
  filterValue = '';
  online = this.network.online;
  private dataLoaded = new Subject();
  private allDataLoaded = 0;
  public mileView = true; // switched in html

  constructor(
              public dialog: MatDialog,
              private tripConfigService: TripConfigService,
              private selectionsService: SelectionsService,
              private moonService: MoonService,
              private sanitizer: DomSanitizer,
              private noteService: NoteService,
              private mapDataService: MapDataService,
              private sunDataService: SunDataService,
              private tideTablesService: TideTablesService,
              protected network: Network) { }


  private static formatDate(date: Date): string {
    return date ? (date.getMonth() + 1).toString().padStart(2, '0')
          + '/' + date.getDate().toString().padStart(2, '0') : '';
  }

  public safetyFirst(svgData: string) {
    svgData = svgData.replace(' rel="noopener noreferrer" target="_blank"', '');
    svgData = svgData.replace(' xlink:href="https://www.icalendar37.net/lunar/app/"', '');
    svgData = svgData.replace('<a>', '');

    const innerHtml = this.sanitizer.bypassSecurityTrustHtml(svgData) as string;
    return innerHtml;
  }
  private getClassWithNote(listItem: ItineraryListItem) {
    return this.getClass(listItem.mapItem);
  }
  private getClass(mapItem: MapItem) {
    let className = '';
    const mapItemType = ItemFiltersService.MapItemTypes.find( t => t.code === mapItem.type);
    if (mapItemType !== undefined) {
      className = mapItemType.class;
      if (this.noteService.hasNote(mapItem.id)) { className += ' hasNote'; }
    }
    return className;
  }
  getSideClass(mapItem: MapItem) {
    let className = '';
    switch (mapItem.side) {
      case 'L':
        className += ' river-left';
        break;
      case 'R':
        className += ' river-right';
        break;
      default:
        className += ' river-center';
        break;
    }
    return className;
  }

  ngOnDestroy() {
    this.svcSub.forEach(s => this.unsub(s));
  }
  private unsub(sub: Subscription) {
    if (sub && !sub.closed) {
      sub.unsubscribe();
    }
  }

  ngOnInit() {
    // Ensure the services are initialized
    const dataLoadingServices = 5;
    this.dataLoaded.subscribe((dataId) => {
      // console.log(`ngOnInit with allDataLoaded = ${this.allDataLoaded} and dataId = ${dataId}`);
      if (++this.allDataLoaded >= dataLoadingServices) {
        this.basicListLoad();
        this.updateItemList();
      }
    });
    this.svcSub.push(this.tripConfigService.serviceData$.subscribe(config => {
      // console.log(`configuration received with configured = ${config.configured}`);
      if (config.configured) {
        this.tripConfig = config;
        this.dataLoaded.next('config');
      }
    }));
    this.svcSub.push(this.moonService.serviceData$.subscribe(moonData => {
      // console.log(`moonData received with a count of ${moonData.length}`);
      this.moonData = moonData;
      this.dataLoaded.next('moondata');
    }));
    this.svcSub.push(this.selectionsService.serviceData$.subscribe(actions => {
      // tslint:disable-next-line:no-string-literal
      if (actions['I10'] !== undefined) { // assumes that Lee's Ferry is selected
        this.actions = actions;
        this.dataLoaded.next('selections');
      }
    }));
    this.svcSub.push(this.mapDataService.serviceData$.subscribe(mapData => {
      // console.log(`mapData received with a count of ${mapData.length}`);
      if (mapData.length > 0) {
        this.mapData = mapData;
        this.dataLoaded.next('mapdata');
      }
    }));
    this.svcSub.push(this.tideTablesService.serviceData$.subscribe(tideTable => {
      this.tides = tideTable;
      this.dataLoaded.next('tides');
    }));
    this.svcSub.push(this.noteService.serviceData$.subscribe(notesData => {
      // console.log(`Itin: Notes updated, updating list of ${this.listItems.length} entries...`);
      this.listItems.forEach(li => {
        li.class = li.class.replace(' hasNote', ''); // Default to no notes.
        // if (li.mapItem.id === 'I890') {
        //   console.log(`item ${li.mapItem.id} has class ${li.class}`);
        // }
        if (notesData.findIndex(nd => nd.id === li.mapItem.id) >= 0) {
          li.class += ' hasNote';
        }
        // if (li.mapItem.id === 'I890') {
        //     console.log(`item ${li.mapItem.id} now has class ${li.class}`);
        // }
      });

    }));
  }
  applyFilter(event: Event) {
    this.basicListLoad();
  }

  basicListLoad() {
    let day = 0;
    let lastCampMile = 0;
    const CAMP = 'C';
    const LAYOVER = 'L';
    // console.log(`itin: initializing this.listItems`);

    this.listItems = [];

    const date = new Date(this.tripConfig.launchDate.valueOf());

    // On this pass, we're just going to load the items so the sequence is what
    // we need it to be, so no async function calls here. (This might have the
    // addtional benefit of painting faster.)
    Object.keys(this.actions)
        .map(s => parseInt(s.substr(1), 10) ) // convert to numeric
        .sort((a, b) => a - b)                // sort the numbers
        .map(n => `I${n}`)                   // put the "I" back on
        .forEach( itemId => {
      const item: MapItem = this.mapData.find( mi => mi.id === itemId );
      if ( this.filterValue.length > 0 && item.name.toLowerCase().search(this.filterValue.toLowerCase()) === -1 ) {
        return;
      }

      const action = this.actions[itemId];

      // create listItem and initialize ActionName
      const listItem: ItineraryListItem = {
        mapItem: item,
        action,
        actionName: SelectionsService.ActionName[action]
      };

      // If this is a camping spot, get the sun and tide data.
      if (action === CAMP || action === LAYOVER) {
        const pd = this.sunDataService.getSunny(date.getMonth(), item.mile);
        if (pd !== undefined) {
          listItem.sunrise = pd.sunrise;
          listItem.sunset = pd.sunset;
          listItem.sunNotes = pd.notes;
        }

        const tide = this.tides.find(t => t.mile === item.mile);
        if (tide !== undefined) {
          // TODO: Right now, we're just pulling the 4mph flow.
          // This can be configured on the config page if needed.
          listItem.tideRises = tide.tides.at4mph.rise;
        }
      }

      // if this is a layover camp, create an extra list entry for the camp
      if (action === LAYOVER) {
        const campItem: ItineraryListItem = {
          mapItem: item,
          actionName: SelectionsService.ActionName.C,
          action: CAMP,
          day: day++
        };
        campItem.dateStr = ItineraryComponent.formatDate(date);
        campItem.date = new Date(date.toISOString());
        date.setDate(date.getDate() + 1);
        campItem.distance = item.mile - lastCampMile;
        lastCampMile = item.mile;
        campItem.class = this.getClass(campItem.mapItem);
        campItem.sideClass = this.getSideClass(campItem.mapItem);
        this.listItems.push(campItem);
      }

      // If this is a camping spot, update the date and set the distance
      if (action === CAMP || action === LAYOVER) {
        listItem.day = day++;
        listItem.date = new Date(date.toISOString());
        listItem.dateStr = ItineraryComponent.formatDate(date);
        date.setDate(date.getDate() + 1);
        listItem.distance = item.mile - lastCampMile;
        lastCampMile = item.mile;
      }

      // Add the item to the UI list;
      listItem.class = this.getClass(listItem.mapItem);
      listItem.sideClass = this.getSideClass(listItem.mapItem);

      // Add the item to the UI list;
      this.listItems.push(listItem);
    });
  }
  updateItemList() {
    const CAMP = 'C';
    const LAYOVER = 'L';

    this.listItems.forEach( (listItem, i) => {
      // If this is a camping spot, add the moon phase info
      if (listItem.action === CAMP || listItem.action === LAYOVER) {
        this.addMoonImage(this.listItems[i]);
      }

      // Update the class to include Note info;
      this.listItems[i].class = this.getClassWithNote(listItem);
    });
  }

  // Filter list
  public search(): void {
    if (this.filterValue.length === 0) {
      const currentFilter = this.filterValue;
      const dialogRef = this.dialog.open(ChooserFilterDialogComponent, {
        data: { filterValue: currentFilter }
      });

      dialogRef.afterClosed().subscribe(newFilter => {
        this.filterValue = newFilter;
        this.applyFilter(null);
      });
    } else {
      this.filterValue = '';
      this.applyFilter(null);
    }
  }

  public goNotes(id: string, name: string, index: number): void {
    // console.log(`Going to notes for ${id}, ${name}, ${index}`);
    const dialogRef = this.dialog.open(NotesComponent, {
      data: { id, name }
    });

    dialogRef.afterClosed().subscribe(noteText => {
      // update the selections list if the comment was the only
      // reason it was displayed.
      if (noteText.length === 0) {
        if (this.listItems[index].action === 'N') {
          this.listItems[index].action = '';
          this.selectionsService.set(id, '');
          // console.log(`Removing item ${id} for a note`);
        }
      }
    });
  }
  private addMoonImage(listItem: ItineraryListItem) {
    const moonData = this.moonData[MoonService.getStorageKey(listItem.date)] as MoonData;
    if (moonData) {
      listItem.moonImage = moonData.phase[listItem.date.getDate()].svg;
    }
  }
}
