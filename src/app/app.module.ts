import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { MaterialDesignModule } from './base-app/material/material-design.module';
import { OverlayContainer } from '@angular/cdk/overlay';
import { AppComponent } from './app.component';
import { ChooserComponent, ChooserFilterDialogComponent } from './trip-itinerary/chooser/chooser.component';
import { ItineraryComponent } from './trip-itinerary/itinerary/itinerary.component';
import { ChooserDetailsComponent } from './trip-itinerary/chooserDetails/chooserDetails.component';
import { NotesComponent } from './trip-itinerary/notes/notes.component';
import { TripLogComponent } from './trip-itinerary/trip-log/trip-log.component';
import { DutyRosterComponent } from './trip-config/duty-roster/duty-roster.component';
import { ExportImportComponent } from './trip-config/export-import/export-import.component';
import { TripConfigComponent } from './trip-config/trip-config/trip-config.component';
import { TideTableComponent } from './trip-config/tide-table/tide-table.component';
import { AboutComponent } from './base-app/about/about.component';
import { IntroComponent } from './base-app/intro/intro.component';
import { IosInstallComponent } from './base-app/ios-install/ios-install.component';
import { NavComponent } from './base-app/nav/nav.component';
import { FofComponent } from './base-app/fof/fof.component';

import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { StorageModule } from '@ngx-pwa/local-storage';
import { DataServicesStaticModule, DataServicesStorageModule } from './services/data-services.module';


const appComponents = [
  AppComponent,
  ChooserComponent,
  ItineraryComponent,
  ChooserDetailsComponent,
  ChooserFilterDialogComponent,
  NotesComponent,
  DutyRosterComponent,
  ExportImportComponent,
  TripConfigComponent,
  TripLogComponent,
  TideTableComponent,
  AboutComponent,
  IntroComponent,
  IosInstallComponent,
  NavComponent,
  FofComponent
];

@NgModule({
  declarations: [...appComponents],
  imports: [
    BrowserAnimationsModule, FormsModule, ReactiveFormsModule,
    HttpClientModule,
    DataServicesStaticModule,
    DataServicesStorageModule,
    MaterialDesignModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production
    }),
    StorageModule.forRoot({ IDBNoWrap: true })
  ],
  bootstrap: [AppComponent],
  entryComponents: [IosInstallComponent]
})
export class AppModule {
  constructor(overlayContainer: OverlayContainer) {
    overlayContainer.getContainerElement().classList.add('canyon-dark-theme');
  }
 }
