import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

import { IntroComponent } from './base-app/intro/intro.component';
import { AboutComponent } from './base-app/about/about.component';
import { TideTableComponent } from './trip-config/tide-table/tide-table.component';
import { ItineraryComponent } from './trip-itinerary/itinerary/itinerary.component';
import { ChooserComponent } from './trip-itinerary/chooser/chooser.component';
import { TripLogComponent } from './trip-itinerary/trip-log/trip-log.component';
import { TripConfigComponent } from './trip-config/trip-config/trip-config.component';
import { DutyRosterComponent } from './trip-config/duty-roster/duty-roster.component';
import { ExportImportComponent } from './trip-config/export-import/export-import.component';
import { FofComponent } from './base-app/fof/fof.component';
import { AppConfiguredGuard } from './services/app-configured.guard';

const routes: Routes = [
  { path: '', redirectTo: 'intro', pathMatch: 'full' },
  { path: 'intro', component: IntroComponent },
  { path: 'config', component: TripConfigComponent },
  {
    path: 'chooser',
    component: ChooserComponent,
    data: { appConfiguredGuardRedirect: '/config' },
    canActivate: [AppConfiguredGuard]
  },
  {
    path: 'itinerary',
    component: ItineraryComponent,
    data: { appConfiguredGuardRedirect: '/config' },
    canActivate: [AppConfiguredGuard]
  },
  { path: 'tide-table', component: TideTableComponent },
  { path: 'trip-log', component: TripLogComponent },
  { path: 'duty-roster', component: DutyRosterComponent },
  { path: 'export-import', component: ExportImportComponent },
  { path: 'about', component: AboutComponent },
  { path: '**', component: FofComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, useHash: true, relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
