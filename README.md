# How to use the app
These is a basic use guide to this Grand Canyon Trip Planner. Hopefully, this page will become obsolete if I can discover a way to make the app more intuitive. Until then, use these steps to setup your trip and enjoy the app.

## Configure
  Launch Date - alters the available sun at certain camps and the phases of the moon.
  Expected flow - alters the time for the rising tide.
  Exchanges, permit, and takeout options - limits options on the Destinations page.
  Dark mode - alters the look of the app.

## Set Destinations
  Touch an item to alter your itinery.
  Touch a campsite twice to indicate a layover day.

## Type filters - limit the types of items in the list.
  All - no additional filters.
  Favorites - a pre-filtered list of "must-see" items
  Other options can be combined to let you focus on certain activities.

## Mile - indicates the river mile of the item.

## Dist - distance between a starting point and the items downstream. Touch the distance column to set the starting point.

## Constraints:
  The items between Diamond and Pearce are filtered out if your take out is Diamond Creek.
  River left camps in the Hualapai territory are disabled if you don't have a Hualapai permit.
  Exchange camps are also disabled near Phantom if you're not exchanging people there.

## Duty Roster
  Configure 4 groups of 4 people assigned to handle the duties for the day.

## Share
  Save your configuration and send it to someone. Or load a saved configuration.
  
# History 
I created a spreadsheet in googlesheets. It was pretty good and I wanted to share it but there was was some "input" required by the user (the month of the trip), and I couldn't figure out a way to share the document in such a way as to allow a user to provide the input without impacting other users. Yes, they could copy the doc, but then any changes that I would subsequently make would not make their way into the copies.

So I decided to create a progressive web app (i.e. one that will function well when the user is offline). This app is the result of that.

# Data
The data used by this app is an accumulation of many sources formatted in a way that is more useful to me, and hopefully you. I don't want to shortchange the creators of the content and tools, so I'm crediting them here.

This is Version 2.17.3, and the online state is true.

# Tide Table
The tide table was created by taking the data on this wiki and making each flow more consistent and then simplifying the output. I have subsequently received permission to update that wiki and have copied that table at the bottom.

# Sunrise and sunset
The sunrise and sunset data was pulled from a document created by Jeff Sorensen, and the copyright is his.
3rd Printing. Copyright 1998 & All Rights Reserved by Jeff Sorensen
JSorensen@azgfd.gov

# Mileages
I updated the mileages to reflect the changes after the 2007 remeasure to match the values in #5
The starting point for the mile map was authored by Elden Saathoff. I was unable to make contact, but I've made significant additions and modifications, including updating the mileages.

# Hikes
The hiking information was pulled from "Day Hikes from the River" 4th Edition by Tom Martin
Mile markers were reset to those found in the "Guide to the Colorado River in the Grand Canyon" 7th Edition by Tom Martin and Duwain Whitis

# PDF to Excell
I must give credit to the PDF to Excel website/service for enabling the conversion of the mile map and sun info pdfs into excel format. There is simply no way that I could (or would) have taken the time to re-enter all of that data. Also, from a technical stand point, I could not have been more shocked to see how well it converted the sun data (it is a scan of booklet).

# Other credits
This also deserves a mention: I use PuffinPaper , 8mil for foldable, double sided printing to print the data from my spreadsheet.
Arron Frost @aaronfrost and Justin Schwartzenberger @scharty! These guys were doing twitch streams and asked what people wanted to see. I suggested a progressive web app, and here I am. Many thanks gents!
Wdisseny.com provides an api for getting the phases of the moon. This is a web service that returns some cool info, but the svg of the moon was so interesting.
Tom Martin had some great suggestions before a 2019 trip, and I'm doing my best to implement some of those changes.

# Suggestions
If you suggestions for improvement, please let me know: bluesotar@hotmail.com
